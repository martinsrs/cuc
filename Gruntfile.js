module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    dist : 'target/dist',
    source : 'target/dist',
    deploy : '../build',

    clean: {
      build : {
        src: [ 'target/' ]
      },
      node_modules: {
        src: [ '<%= source %>/node_modules' , '<%= source %>/package.json', '<%= source %>/Gruntfile.js', '<%= source %>/_templates']
      },
      stylesheets: {
        src: [ '<%= dist %>/css/*.css', '!<%= dist %>/css/<%= pkg.name %>.min.css' ]
      },
      scripts: {
        src: [ '<%= dist %>/scripts/**/*.js', '!<%= dist %>/scripts/<%= pkg.name %>.min.js' ]
      },
    },

    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['<%= source %>/scripts/*.js', '<%= source %>/scripts/**/*.js'],
        dest: '<%= dist %>/scripts/<%= pkg.name %>.js'
      }
    },    

    copy: {
      build: {
        cwd: '',
        src: [ '**', '!**/*.styl', '!**/node_modules/**', '!gitignore', '!**/_templates/**', '!**/build/**', '!**/Gruntfile.js', '!**/package.json', '!**/README.md', '!**/LICENCE' ],
        dest: '<%= source %>',
        expand: true
      },
      deploy: {
        cwd: '<%= dist %>',
        src: [ '**' ],
        dest: '../build',
        expand: true
      },
    },

    cssmin: {
      build : {
        files: {
          '<%= dist %>/css/<%= pkg.name %>.min.css': [ '<%= source %>/css/*.css' ]
        }
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },

      build : {
        src: ['<%= source %>/scripts/<%= pkg.name %>.js'],
        dest: '<%= dist %>/scripts/<%= pkg.name %>.min.js'
      }
    },

    useminPrepare: {
      html: ['<%= dist %>/index.html'],
      options: {
        dest: '<%= dist %>/'
      }
    },

    usemin: {
      html: ['<%= dist %>/index.html'],
      css: ['<%= dist %>/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= dist %>/']
      }
    }

  });

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-usemin');

  // Default task(s).
  grunt.registerTask('clear', 'Clean build assets', ['clean:build']);
  grunt.registerTask('stylesheets', 'Compiles the stylesheets.', ['cssmin','clean:stylesheets']);
  grunt.registerTask('scripts', 'Compiles the JavaScript files.', [ 'uglify','clean:scripts']);
  grunt.registerTask('build', 'Compiles all of the assets and copies the files to the build directory.', ['clean:build','copy:build','concat','scripts', 'stylesheets', 'usemin']);
  grunt.registerTask('default', ['build']);

};