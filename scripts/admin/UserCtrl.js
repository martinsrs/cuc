'use strict';

/**
 * @ngdoc function
 * @name cucApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the cucApp
 */ 
angular.module('cucApp').controller('UserCtrl', UserCtrl);

UserCtrl.$inject = ['$scope', '$rootScope', '$route', '$location', 'AppConfig', 'UserService']

function UserCtrl($scope, $rootScope, $route, $location, AppConfig, UserService) {

	this.isLogged = false;
	this.currentUser = false;

	var t = this;
	UserService.isLogged(function (data) {
		t.isLogged = data;
	});

	UserService.getLoggedUser(function(loggedUser) {
		if (loggedUser != null) {
			UserService.getUser(loggedUser.idUsuario, function(result) {
				t.currentUser = result;
			});
		}	
	});

	this.saveUser = function(user) {

		if (user.tmpSenha != "" && user.tmpSenha.trim() != "") {
			if (user.tmpSenha == user.tmpSenha2) {

				user.senha = user.tmpSenha;
				UserService.saveUser(user, function(result){
				});
				
			}
		}
	}

	this.disableSave = function() {

		var disabled = false;
		if (t.currentUser.tmpSenha == null || t.currentUser.tmpSenha == "" || t.currentUser.tmpSenha.trim() == "") {
			disabled = true;
		}

		if (t.currentUser.tmpSenha != t.currentUser.tmpSenha2) {
			disabled = true;
		}

		return disabled;
	}

}
