'use strict';

/**
 * @ngdoc function
 * @name cucApp.controller:AdminCtrl
 * @description
 * # AdminCtrl
 * Controller of the cucApp
 */ 
angular.module('cucApp').controller('AdminCtrl', AdminCtrl);

AdminCtrl.$inject = ['$scope', '$rootScope', '$route', '$location', 'AppConfig', 'UserService']

function AdminCtrl($scope, $rootScope, $route, $location, AppConfig, UserService) {

	this.isLogged = false;

	var t = this;
	UserService.isLogged(function (data) {
		t.isLogged = data;
	});

	this.cancel = function() {
		$location.path('/');
	}

	this.logout = function() {
		UserService.logout();
	}

	this.login = function(user)	{

		var t = this;
		UserService.loginUser(user, function(data) {
			
			if (data.length > 0) {
				document.getElementById('formLogin').action = "#/admin/profile";
				document.getElementById('formLogin').submit();
			} else {
				t.user.senha = "";
				t.loginError = true;
			}
		});
	}

}
