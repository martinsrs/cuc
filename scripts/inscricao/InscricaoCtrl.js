'use strict';

angular.module('cucApp').controller('InscricaoCtrl', InscricaoCtrl);

InscricaoCtrl.$inject = ['$scope', '$route', 'AppConfig', 'AtletaService', 'CategoriaService'];

function InscricaoCtrl($scope, $route, AppConfig, AtletaService, CategoriaService) {
 
    var t = this;
    this.vo = { atleta : false };

    $scope.$watch('atleta', function() {
        t.vo.atleta = $scope.atleta;
        t.vo.categoriasSelecionadas = null;
        if (t.vo.atleta && t.vo.atleta != null) {
            AtletaService.listInscricaoAtleta(t.vo.atleta.idAtleta, t.vo.idEtapa, function(data) {
                angular.forEach(data, function (value, key) {

                    if (t.vo.categoriasSelecionadas == null) {
                        t.vo.categoriasSelecionadas = [];
                    }
                    var toAdd = value.categoria;
                    toAdd.numero = value.numero;                

                    t.vo.categoriasSelecionadas.push(toAdd);
                });
            });
        }
    });
        
    this.disableInscricao = function() {
        var ret = true;
        if (this.vo.atleta != false) {
            if (t.vo.categoriasSelecionadas != null) {
                var emptyNumbers = false;
                angular.forEach(t.vo.categoriasSelecionadas, function (value, key) {
                    if (value.numero == null || value.numero == "") {
                        emptyNumbers = true;
                    }
                });
                ret = emptyNumbers;
            } else {
                ret = false;
            }
        }
        return ret;
    }
    
    this.cancelar = function(callback) {
        this.desmarcaAtleta();
        this.vo = { atleta : false };
        $scope.pesquisanome = "";
    }
    
    this.desmarcaAtleta = function(callback) {
        $scope.atleta = false; 
        t.vo.atleta = false;
        this.atletaList = false;
        $scope.pesquisanome = "";
    };
    
    this.inscrever = function(vo, callback) {
        var t= this;
        AtletaService.inscreverAtleta(vo,function() {
            t.desmarcaAtleta();
        });
    }
}