'use strict';

/**
 * @ngdoc overview
 * @name cucApp
 * @description
 * # cucApp
 *
 * Main module of the application.
 */
angular.module('cucApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'textAngular'
  ]).config(MainApp);

MainApp.$inject = ['$routeProvider', '$provide'];

function MainApp($routeProvider, $provide) {
    
  $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions) { // $delegate is the taOptions we are decorating
                  taOptions.toolbar = [
                        ['h1', 'h2', 'h3', 'h6', 'p', 'quote'],
                        ['bold', 'italics', 'underline', 'strikeThrough', 'ul', 'ol', 'clear'],
                        ['justifyLeft', 'justifyCenter', 'justifyFull'],
                        ['html', 'insertImage','insertLink']
                    ];
      
                  return taOptions;
              }]);

  $routeProvider
    .when('/', {
      templateUrl: 'scripts/main/MainView.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })
    .when('/temporada/:id', {
      templateUrl: 'scripts/temporada/TemporadaView.html',
      controller: 'TemporadaCtrl',
      controllerAs: 'temporada'
    })
    .when('/admin', {
      templateUrl: 'scripts/user/LoginView.html',
      controller: 'AdminCtrl',
      controllerAs: 'admin'
    })
    .when('/admin/profile', {
      templateUrl: 'scripts/user/UserProfile.html',
      controller: 'AdminCtrl',
      controllerAs: 'admin'
    })
    .when('/admin/logout', {
      templateUrl: 'scripts/user/LoginView.html',
      controller: 'AdminCtrl',
      controllerAs: 'admin'
    })
    .when('/admin/temporada', {
      templateUrl: 'scripts/temporada/TemporadaEdit.html',
      controller: 'TemporadaCtrl',
      controllerAs: 'temporada'
    })
    .when('/admin/categoria', {
      templateUrl: 'scripts/categoria/CategoriaEdit.html',
      controller: 'CategoriaCtrl',
      controllerAs: 'categoria'
    })
    .when('/admin/etapa', {
      templateUrl: 'scripts/etapa/EtapaEdit.html',
      controller: 'EtapaCtrl',
      controllerAs: 'etapa'
    })
    .when('/admin/etapa/resultados/:idTemporada/:idEtapa', {
      templateUrl: 'scripts/etapa/resultados/ResultadosView.html',
      controller: 'EtapaCtrl',
      controllerAs: 'resultados'
    })
    .when('/admin/etapa/largadas/:idTemporada/:idEtapa', {
      templateUrl: 'scripts/etapa/largadas/LargadaView.html',
      controller: 'LargadaCtrl',
      controllerAs: 'largada'
    })
    .when('/admin/equipe', {
      templateUrl: 'scripts/equipe/EquipeEdit.html',
      controller: 'EquipeCtrl',
      controllerAs: 'equipe'
    })
    .when('/admin/atleta', {
      templateUrl: 'scripts/atleta/AtletaEdit.html',
      controller: 'AtletaCtrl',
      controllerAs: 'atleta'
    })
    .when('/admin/inscricao', {
      templateUrl: 'scripts/inscricao/InscricaoView.html',
      controller: 'InscricaoCtrl',
      controllerAs: 'inscricao'
    })
    .otherwise({
      redirectTo: '/'
    });
  };   