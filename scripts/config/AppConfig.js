'use strict';

/**
 * @ngdoc function
 * @name blogApp.service:AppConfig
 * @description
 * # AppConfig
 * Controller of the blogApp
 */
angular.module('cucApp').factory('AppConfig', AppConfig);
    
function AppConfig() {
    return {
        indexUrl : 'http://localhost:81/cuc',
        USER_SESSION_KEY : 'loggedUser',
        previewPostLength : 220,
        dateLocale: 'pt-BR',
        dateFormat: {
            default : 'YYYY-MM-DD',
            long  : 'dddd, DD [de] MMMM [de] YYYY',
            short : 'DD/MM/YYYY'
        }
    }
}