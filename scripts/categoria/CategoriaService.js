'use strict';

/**
 * @ngdoc function
 * @name cucApp.service:CategoriaService
 * @description
 * # CategoriaService
 */
angular.module('cucApp').service('CategoriaService', CategoriaService);

CategoriaService.$inject = ['$http', 'AppConfig'];

function CategoriaService($http, AppConfig) {
    
    this.getAllCategoriaList = function (callback) {
        
        $http.get("core/categoria/categoria.php", {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            }
        });    
    }
    
    this.getCategoriaById = function (id, callback) {
        
        $http.get("core/categoria/categoria.php?id_categoria=" + id, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            }
        });    
    }
    
    this.getAllCategoriaWithResultList = function (vo, callback) {
        
        var filter = "?id_temporada=" + vo.idTemporada;
        if (vo.idEtapa != null) {
            filter += "&id_etapa=" + vo.idEtapa;
        }
        if (vo.idCategoria != null) {
            filter += "&id_categoria=" + vo.idCategoria;
        }

        $http.get("core/categoria/listcategoriaresult.php" + filter, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            }
        });    
    }

    this.getAllCategoriaByTemporada = function (idTemporada, callback) {
        
        $http.get("core/categoria/categoria.php?byTemporada=" + idTemporada, {}).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                
                var categoriaList = [];
                for(var i=0; i<result.data.length; i++) {
                    
                    var categoriaVo = {
                        idCategoria : result.data[i].id_categoria,
                        nome : result.data[i].nome,
                        criterio  : result.data[i].criterio,
                        ordem  : result.data[i].ordem,
                        checked : result.data[i].checked
                    }
                    categoriaList.push(categoriaVo);
                }
                return callback(categoriaList);
            }
        });
    }

    this.getAllCategoriaByTemporadaOrderName = function (idTemporada, callback) {
        
        $http.get("core/categoria/categoria.php?combo=true&byTemporada=" + idTemporada, {}).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                
                var categoriaList = [];
                for(var i=0; i<result.data.length; i++) {
                    
                    var categoriaVo = {
                        idCategoria : result.data[i].id_categoria,
                        nome : result.data[i].nome,
                        criterio  : result.data[i].criterio,
                        ordem  : result.data[i].ordem,
                        checked : result.data[i].checked
                    }
                    categoriaList.push(categoriaVo);
                }
                return callback(categoriaList);
            }
        });
    }
    
    this.getAllCategoriaByTemporadaSigned = function (idTemporada, idEtapa, callback) {
        
        $http.get("core/categoria/categoria.php?byTemporada=" + idTemporada + "&id_etapa="+ idEtapa +"&onlySigned=true", {}).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                
                var categoriaList = [];
                for(var i=0; i<result.data.length; i++) {
                    
                    var categoriaVo = {
                        idCategoria : result.data[i].id_categoria,
                        nome : result.data[i].nome,
                        criterio  : result.data[i].criterio,
                        ordem  : result.data[i].ordem,
                        checked : result.data[i].checked
                    }
                    categoriaList.push(categoriaVo);
                }
                return callback(categoriaList);
            }
        });
    }

    this.getAllCategoriaByTemporadaSignedOrderNome = function (idTemporada, idEtapa, callback) {
        
        $http.get("core/categoria/categoria.php?combo=true&byTemporada=" + idTemporada + "&id_etapa="+ idEtapa +"&onlySigned=true", {}).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                
                var categoriaList = [];
                for(var i=0; i<result.data.length; i++) {
                    
                    var categoriaVo = {
                        idCategoria : result.data[i].id_categoria,
                        nome : result.data[i].nome,
                        criterio  : result.data[i].criterio,
                        ordem  : result.data[i].ordem,
                        checked : result.data[i].checked
                    }
                    categoriaList.push(categoriaVo);
                }
                return callback(categoriaList);
            }
        });
    }

    this.listCategoriaByTemporadaChecked = function(idTemporada, callback) {
        $http.get("core/categoria/listcategoria.php?id_temporada=" + idTemporada, {}).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            } else {
                return callback([]);
            }
        });
    }
    
    this.deleteCategoria = function (categoriaVo, callback) {
        $http.post("core/categoria/deleteCategoria.php", categoriaVo).then(function(result) {
            return callback();
        });
    }
    
    this.saveCategoria = function (categoriaVo, callback) {
        $http.post("core/categoria/saveCategoria.php", categoriaVo).then(function(result) {
            return callback();
        });
    }
    
    this.moveUp = function (vo, callback) {
        vo.direction = "up";        
        $http.post("core/categoria/moveCategoria.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.moveDown = function (vo, callback) {
        vo.direction = "down";        
        $http.post("core/categoria/moveCategoria.php", vo).then(function(result) {
            return callback();
        });
    }
}