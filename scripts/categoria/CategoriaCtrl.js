'use strict';

/**
 * @ngdoc function
 * @name cucApp.controller:CategoriaCtrl
 * @description
 * # CategoriaCtrl
 * Controller of the cucApp
 */ 
angular.module('cucApp')
  .controller('CategoriaCtrl', CategoriaCtrl);

CategoriaCtrl.$inject = ['$scope', '$route', 'AppConfig', 'CategoriaService'];

function CategoriaCtrl($scope, $route, AppConfig, CategoriaService) {

    this.categotiaVo;
    
    this.disableSave = function() {
        if (this.categoriaVo != null) {
            if (this.categoriaVo.nome != null && this.categoriaVo.criterio != null && this.categoriaVo.valorInscricao != null) {
                return false;
            }
        }
            
        return true;
    }
    
    this.saveCategoria = function (vo) {
        CategoriaService.saveCategoria(vo, function(){
        });
    }
}