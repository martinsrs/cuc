'use strict';

angular.module('cucApp').directive('categoriaSignUp', CategoriaSignUp);

function CategoriaSignUp() {

	return {
		templateUrl: 'scripts/categoria/directive/CategoriaSignUp.html',
		restrict: 'E',
        scope : {
            ngModel : '=',
            formAction : '@',
            idTemporada : '='
        },
		controller: CategoriaSignUpCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

CategoriaSignUpCtrl.$inject = ['$scope', 'CategoriaService'];

function CategoriaSignUpCtrl($scope, CategoriaService) {

    $scope.addCategoria = function(id, callback) {
        var exists = false;
        angular.forEach($scope.ngModel, function (value, key) {
            if (value.idCategoria == id) {
                exists = true;
            }
        });
        
        if (!exists) {
            CategoriaService.getCategoriaById(id, function(data){
                angular.forEach(data, function (value, key) {
                    if ($scope.ngModel == null) {
                        $scope.ngModel = [];
                    }
                    $scope.ngModel.push(value);
                });
            });
        }    
    }
    
    $scope.delCategoria = function(vo, callback) {
        
        var idToRemove = vo.idCategoria;
        
        angular.forEach($scope.ngModel, function (value, key) {
            if (value.idCategoria == idToRemove) {
                $scope.ngModel.splice(key, 1);
            }
        });
        
        if ($scope.ngModel.length == 0) {
            $scope.ngModel = null;
        }
    }

}