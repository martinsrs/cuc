'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:CategoriaCombo
 * @description
 * # CategoriaCombo
 */
angular.module('cucApp').directive('categoriaCombo', CategoriaCombo);

function CategoriaCombo() {

	return {
		template: '<select class="form-control" ng-model="ngModel" ng-disabled="ngDisabled">' +
                    '<option value="" disabled selected>Selecione uma categoria...</option>' +
                    '<option ng-repeat="categoriaVo in categoriaAllList" value="{{categoriaVo.idCategoria}}">{{categoriaVo.nome}}</option>' +
                  '</select>',
		restrict: 'E',
        scope : {
            ngModel : '=',
            ngDisabled : '=',
            temporada : '=',
            idTemporada : '=',
            idEtapa     : '=',
            onlySigned : '@'
        },
		controller: CategoriaComboCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

CategoriaComboCtrl.$inject = ['$scope', 'CategoriaService'];

function CategoriaComboCtrl($scope, CategoriaService) {
    
    var t = this;    
    
    $scope.categoriasSelecionadas = [];
    $scope.categoriaAllList = [];
    
    $scope.$watch('idTemporada', function() {
        $scope.categoriaAllList = [];
        if ($scope.idTemporada != null) {
            
            if ($scope.onlySigned) {
                t.getAllCategoriaByTemporadaSigned($scope.idTemporada, $scope.idEtapa);
            } else {
                t.getAllCategoriaByTemporada($scope.idTemporada);
            }
        }
    });
    
    $scope.$watch('idEtapa', function() {
        $scope.categoriaAllList = [];
        if ($scope.idTemporada != null) {
            if ($scope.onlySigned) {
                t.getAllCategoriaByTemporadaSigned($scope.idTemporada, $scope.idEtapa);
            } else {
                t.getAllCategoriaByTemporada($scope.idTemporada);
            }
        }
    });
    
    $scope.$watch('temporada', function() {  
        $scope.categoriaAllList = [];
        if ($scope.temporada != null && $scope.temporada.idTemporada != null) {            
            if ($scope.onlySigned) {
                t.getAllCategoriaByTemporadaSigned($scope.temporada.idTemporada, $scope.idEtapa);
            } else {
                t.getAllCategoriaByTemporada($scope.temporada.idTemporada);
            }
            
        }
    });
    
    this.getAllCategoriaByTemporadaSigned = function(idTemporada, idEtapa) {
        CategoriaService.getAllCategoriaByTemporadaSignedOrderNome(idTemporada, idEtapa, function(data){    
            $scope.categoriaAllList = data;
        });     
    };
    
    this.getAllCategoriaByTemporada = function(id) {
        
        CategoriaService.getAllCategoriaByTemporadaOrderName(id, function(data){    
        
            $scope.categoriaAllList = [];
            $scope.categoriasSelecionadas = [];                
            angular.forEach(data, function (value, key) {
                if (value.checked) {
                    $scope.categoriaAllList.push(value);
                }
            });
        });     
    };
    
}