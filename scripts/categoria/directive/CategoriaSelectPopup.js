'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:CategoriaSelectPopup
 * @description
 * # CategoriaSelectPopup
 */
angular.module('cucApp').directive('categoriaSelectPopup', CategoriaSelectPopup);

function CategoriaSelectPopup() {

	return {
		templateUrl: 'scripts/categoria/directive/CategoriaSelectPopup.html',
		restrict: 'E',
        scope : {
            
            temporada : '='
            
        },
		controller: CategoriaSelectPopupCtrl,
		link: function (scope, elem, attr) {
		}
    };
}

CategoriaSelectPopupCtrl.$inject = ['$scope', 'CategoriaService', 'TemporadaService'];

function CategoriaSelectPopupCtrl($scope, CategoriaService, TemporadaService) {
    
    var t = this;
    
    $scope.categoriasSelecionadas = [];
        
    $scope.$watch('temporada', function() {
        if ($scope.temporada != null && $scope.temporada.idTemporada != null) {            
            CategoriaService.getAllCategoriaByTemporada($scope.temporada.idTemporada, function(data){        
                $scope.categoriaAllList = data;
                t.categoriaAllList = data;
                
                $scope.categoriasSelecionadas = [];                
                angular.forEach($scope.categoriaAllList, function (value, key) {
                    if (value.checked) {
                        $scope.categoriasSelecionadas[value.idCategoria] = value.checked;
                    }
                });
            });
        }
    });
    
    $scope.salvar = function() {
        
        var listSelecionadas = [];        
        angular.forEach($scope.categoriasSelecionadas, function (value, key) {
            if (value == true || value == 1) {
                listSelecionadas.push(key);
            }
        });
        
        var vo = {
            idTemporada : $scope.temporada.idTemporada,
            categorias : listSelecionadas
        }
        
        TemporadaService.assignCategorias(vo, function(data){
        });
    }
    
}