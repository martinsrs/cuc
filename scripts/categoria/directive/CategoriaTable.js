'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:CategoriaTable
 * @description
 * # CategoriaTable
 */
angular.module('cucApp').directive('categoriaTable', CategoriaTable);

function CategoriaTable() {

	return {
		templateUrl: 'scripts/categoria/directive/CategoriaTable.html',
		restrict: 'E',
        scope : {
            
        },
		controller: CategoriaTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

CategoriaTableCtrl.$inject = ['$scope', 'CategoriaService'];

function CategoriaTableCtrl($scope, CategoriaService) {
    var t = this;
    
    CategoriaService.getAllCategoriaList(function(data){
        $scope.categoriaAllList = data;
        t.categoriaAllList = data;
    });
    
    $scope.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }
    
    $scope.save = function(vo) {     
        CategoriaService.saveCategoria(vo, function () {
            vo.edit = false;
        });
    }
    
    $scope.moveUp = function(vo) {
        CategoriaService.moveUp(vo, function () {
            
            CategoriaService.getAllCategoriaList(function(data){
                $scope.categoriaAllList = data;
                t.categoriaAllList = data;
            });
            
        });
    }
    
     $scope.moveDown = function(vo) {
        CategoriaService.moveDown(vo, function () {
            
            CategoriaService.getAllCategoriaList(function(data){
                $scope.categoriaAllList = data;
                t.categoriaAllList = data;
            });
            
        });
    }
    
    $scope.prepareDel = function(vo) {
        $scope.toDeleteVo = vo;
    }
    
    $scope.delete = function(vo) {
        CategoriaService.deleteCategoria(vo, function () {
            
            CategoriaService.getAllCategoriaList(function(data){
                $scope.categoriaAllList = data;
                t.categoriaAllList = data;
            });
            
        });
    }
}