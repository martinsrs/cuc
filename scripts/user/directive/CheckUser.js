'use strict';

angular.module('cucApp').directive('checkUser', CheckUser);

function CheckUser() {
	return {
		template: '',
		restrict: 'E',
        scope : {
            ngModel : '='
        },
		controller: CheckUserCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

CheckUserCtrl.$inject = ['$scope', '$location', 'UserService'];

function CheckUserCtrl($scope, $location, UserService) {

    UserService.isLogged(function(logged) {
        if (!logged) {
            $location.path('/admin');    
        }
    });

}