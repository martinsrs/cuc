'use strict';

angular.module('cucApp').service('UserService',  UserService);

UserService.$inject = ['$resource', '$cookieStore','$templateCache', '$http', 'AppConfig'];

function UserService($resource, $cookieStore, $templateCache, $http, AppConfig) {
	AppConfig.USER_SESSION_KEY;

  	this.getLoggedUser = function(callback) {
  		var loggedUser = null;

  		this.isLogged(function(logged) {
	  		loggedUser = $cookieStore.get(AppConfig.USER_SESSION_KEY);
			return callback(loggedUser);
  		});

  	}

  	this.isLogged = function(callback) {
  		return callback($cookieStore.get(AppConfig.USER_SESSION_KEY) != null);
  	}

  	this.getUser = function(id, callback) {
  		$http.get("core/usuario/usuario.php?id_usuario=" + id).then(function(result){

  			if (result.data != null && result.data.length > 0) {
  				return callback(result.data[0]);
  			}
  		});
  	}


  	this.saveUser = function (user, callback) {

		var t= this;
		$http.post("core/usuario/saveUsuario.php", user).then(function(result) {

  			$templateCache.removeAll();
			if (result.data != null && result.data.length > 0) {
				$cookieStore.put(AppConfig.USER_SESSION_KEY, user);
  				return callback(result.data[0]);
  			}

		});

	}


  	this.loginUser = function (user, callback) {
  		$templateCache.removeAll();

		var t= this;
		$http.post("core/usuario/login.php", user).then(function(result) {

			if (result != null && result.data.length > 0) {
				$cookieStore.put(AppConfig.USER_SESSION_KEY, result.data[0]);
			} else {
				t.logout();
				return callback([]);
			}

		    return callback(result.data);
		});

	}

	this.logout = function() {
		$templateCache.removeAll();
		$cookieStore.remove(AppConfig.USER_SESSION_KEY);
	}

  }