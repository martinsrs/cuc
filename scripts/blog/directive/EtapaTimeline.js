'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:EtapaTimeline
 * @description
 * # EtapaTimeline
 */
angular.module('cucApp').directive('etapaTimeline', EtapaTimeline);

function EtapaTimeline() {

	return {
		templateUrl: 'scripts/blog/directive/EtapaTimeline.html',
		restrict: 'E',
        scope : {
            paginate : '@',
            pageSize : '@',
            currentPage : '='
        },
		controller: EtapaTimelineCtrl,
		link: function (scope, elem, attr) {

		}
    };
}

EtapaTimelineCtrl.$inject = ['$scope', 'AppConfig', 'EtapaService', 'TemporadaService'];

function EtapaTimelineCtrl($scope, AppConfig, EtapaService, TemporadaService) {
    
    $scope.listTimeline = null;

    $scope.filter = {
        paginate : $scope.paginate,
        pageSize : $scope.pageSize,
        currentPage : $scope.currentPage
    }

    $scope.last = false;

    $scope.$watch('filter + ";" + filter.currentPage', function() {

        TemporadaService.getTemporadaEtapa($scope.filter, function(data) {

            angular.forEach(data.list, function (temporada, key) {
                angular.forEach(temporada.etapas, function (etapa, key) {
                    var dataBullet = moment(etapa.data).format('DD/MM');
                    var dataFormat = moment(etapa.data).format(AppConfig.dateFormat.short);
                    etapa.dataBullet = dataBullet;
                    etapa.data = dataFormat;
                });
            });

            var t = data.totalRows - (data.pageSize * data.currentPage);
            if (t <= data.pageSize) {
                $scope.last = true;
            } else {
                $scope.last = false;
            }

            $scope.listTimeline = data;
        });
    });


    $scope.paginateLeft = function() {
        if ($scope.filter.currentPage > 0) {
            $scope.filter.currentPage--;
        }
    }

    $scope.paginateRight = function() {
        $scope.filter.currentPage++;            
    }


}