'use strict';

/**
 * @ngdoc function
 * @name cucApp.service:TemporadaService
 * @description
 * # EtapaService
 */
angular.module('cucApp').service('EtapaService', EtapaService);

EtapaService.$inject = ['$http', 'AppConfig'];

function EtapaService($http, AppConfig) {

    this.getEtapaList = function (callback) {
        $http.get("core/etapa/etapa.php", {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                var list = result.data;
                return callback(list);
            }
        });    
    }

    this.getEtapaListByTemporada = function (vo, callback) {
        $http.get("core/etapa/etapa.php?id_temporada=" + vo.idTemporada, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                var list = result.data;
                return callback(list);
            }
        });    
    }
    
    this.getEtapaByTemporadaAndId = function (vo, callback) {
        $http.get("core/etapa/etapa.php?id_temporada=" + vo.idTemporada + '&id_etapa=' + vo.idEtapa, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                var list = result.data;
                return callback(list);
            }
        });    
    }

    this.getLargadasByEtapa = function (vo, callback) {
        $http.get("core/etapa/largada.php?id_temporada=" + vo.temporada.idTemporada + '&id_etapa=' + vo.idEtapa, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                var list = result.data;
                return callback(list);
            }
        }); 
    }

    this.getEtapaListByTemporadaInscricao = function (vo, callback) {
        $http.get("core/etapa/etapa.php?id_temporada=" + vo.idTemporada + '&inscricao=true', {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                var list = result.data;
                return callback(list);
            }
        });    
    }
    
    this.saveEtapa = function (vo, callback) {        
        $http.post("core/etapa/saveEtapa.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.deleteEtapa = function (vo, callback) {        
        $http.post("core/etapa/deleteEtapa.php", vo).then(function(result) {
            return callback();
        });    
    }

    this.publicarResultados = function (vo, callback) {        
        
        var tmpVo = {
            idTemporada : vo.temporada.idTemporada,
            idEtapa : vo.idEtapa
        }
        this.getEtapaByTemporadaAndId(tmpVo, function(data) {
            vo = data[0];
            vo.publicarResultado = 1;

            $http.post("core/etapa/saveEtapa.php", vo).then(function(result) {
                return callback();
            });
        });
    }

    this.despublicarResultados = function (vo, callback) {
        var tmpVo = {
            idTemporada : vo.temporada.idTemporada,
            idEtapa : vo.idEtapa
        }
        this.getEtapaByTemporadaAndId(tmpVo, function(data) {
            vo = data[0];
            vo.publicarResultado = 0;

            $http.post("core/etapa/saveEtapa.php", vo).then(function(result) {
                return callback();
            });
        });
    }

    this.saveLargadas = function (vo, callback) {   
        $http.post("core/etapa/saveLargadas.php", vo).then(function(result) {
            return callback();
        });
    }

    this.copyLargadas = function (vo, callback) {   
        $http.post("core/etapa/copyLargadas.php", vo).then(function(result) {
            return callback();
        });
    }

    this.saveEtapa = function (vo, callback) {        
        $http.post("core/etapa/saveEtapa.php", vo).then(function(result) {
            return callback();
        });
    }
}