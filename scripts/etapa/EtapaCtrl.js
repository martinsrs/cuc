'use strict';

/**
 * @ngdoc function
 * @name cucApp.controller:EtapaCtrl
 * @description
 * # EtapaCtrl
 * Controller of the cucApp
 */ 
angular.module('cucApp')
  .controller('EtapaCtrl', EtapaCtrl);

EtapaCtrl.$inject = ['$scope', '$routeParams', 'AppConfig', 'CategoriaService', 'EtapaService', 'AtletaService'];

function EtapaCtrl($scope, $routeParams, AppConfig, CategoriaService, EtapaService, AtletaService) {

    this.listClassificacao = false;
    this.currentEtapa = {};
    this.categotiaVo;
    this.saved = false;
    
    this.disableSave = function() {
        if (this.categoriaVo != null) {
            if (this.categoriaVo.nome != null && this.categoriaVo.criterio != null) {
                return false;
            }
        }
            
        return true;
    }
    
    var t = this;
    $scope.$watch("idCategoria", function() {
        t.saved = false;
        if (t.currentEtapa.idEtapa != null && t.currentEtapa.temporada.idTemporada != null & $scope.idCategoria != null) {
            var vo = t.getVo(t.currentEtapa.temporada.idTemporada, t.currentEtapa.idEtapa, $scope.idCategoria);
            t.updateTable(vo);
        }
    });


    this.saveCategoria = function (vo) {
        CategoriaService.saveCategoria(vo);
    }

    this.getVo = function (idTemporada, idEtapa, idCategoria) {
        var vo = {
            idTemporada : idTemporada,
            idEtapa     : idEtapa,
            idCategoria : idCategoria
        }
        return vo;
    }

    this.getEtapa = function (vo) {
        var t = this;
        if (vo != null) {
            EtapaService.getEtapaByTemporadaAndId(vo, function(data) {
                moment.locale(AppConfig.dateLocale);
                angular.forEach(data, function(value, key) {
                    t.currentEtapa = value;
                    t.currentEtapa.dataLong = moment(value.data).format(AppConfig.dateFormat.long);
                });
            });
        }
    }

    this.updateTable = function (vo) {        
        $scope.listClassificacao = false;
        var t = this;
        AtletaService.listClassificacao(vo.idTemporada, vo.idEtapa, vo.idCategoria, function(data) {
            t.listClassificacao = data;
        });   
    }


    this.disableSaveResultados = function() {
        
        var result = false;
        if (this.currentEtapa != null && this.currentEtapa.publicarResultado) {
            result = true;
        } else if (this.listClassificacao != false) {
            angular.forEach(this.listClassificacao, function(value, key) {
               if (value.classificacao == null || value.classificacao == "" || value.classificacao <= 0) {
                   result = true;
               }
            });
        } else {
            result = true;
        }
        
        return result;
    }
    
    this.salvarResultados = function() {
        var t = this;
        AtletaService.saveClassificacao(this.listClassificacao, function () {
            var vo = t.getVo(t.currentEtapa.temporada.idTemporada, t.currentEtapa.idEtapa, $scope.idCategoria);
            t.updateTable(vo);
            t.saved = true;
        });
    }

    if ($routeParams != null) {
        if ($routeParams.idTemporada != null && $routeParams.idEtapa != null) {
            var idTemporada =$routeParams.idTemporada;
            var idEtapa = $routeParams.idEtapa;  

            var vo = this.getVo(idTemporada, idEtapa, "");

            this.getEtapa(vo);
            this.updateTable(vo);
        }
    }
}