'use strict';

angular.module('cucApp').directive('largadaTable', LargadaTable);

function LargadaTable() {

	return {
		templateUrl: 'scripts/etapa/largadas/directive/LargadaTable.html',
		restrict: 'E',
        scope : {
            etapa : '=',
            temporada : '='
        },
		controller: LargadaTableCtrl,
		link: function (scope, elem, attr) {

		}
    };
}

LargadaTableCtrl.$inject = ['$scope', 'EtapaService'];

function LargadaTableCtrl($scope, EtapaService) {

	$scope.horarioLargadas = false;

	if ($scope.etapa != null) {
		if ($scope.etapa.temporada == null && $scope.temporada != null) {
			$scope.etapa.temporada = $scope.temporada;
			EtapaService.getLargadasByEtapa($scope.etapa, function(data) {
				$scope.largadas = data;
				if (data.length > 0) {
					if (data[0].largadas.length > 0) {

						var old_horario = "";
						var tmpLargada;
						var horarioLargadas = [];
						angular.forEach(data[0].largadas, function (item, key) {
							if (old_horario != item.horarioLargada) {

								if (old_horario != "") {
									horarioLargadas.push(tmpLargada);
								}

								tmpLargada = {
									horarioLargada : item.horarioLargada,
									listCategorias : []
								}
								old_horario = item.horarioLargada;
							}

							item.categoria.tempoProva = item.tempoProva;
							tmpLargada.listCategorias.push(item.categoria);
						});
						horarioLargadas.push(tmpLargada);

						$scope.horarioLargadas = horarioLargadas;
					} else {
						$scope.horarioLargadas = false;
					}
				}
			});
		}
	}

	
}