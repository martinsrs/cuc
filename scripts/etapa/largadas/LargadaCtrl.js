'use strict';

angular.module('cucApp').controller('LargadaCtrl', LargadaCtrl);

LargadaCtrl.$inject = ['$scope', '$routeParams', 'AppConfig', 'CategoriaService', 'EtapaService', 'AtletaService'];

function LargadaCtrl($scope, $routeParams, AppConfig, CategoriaService, EtapaService, AtletaService) {

    this.currentEtapa = {};
    this.saved = false;
    this.categoriaList = false;

    this.copiarLargadas = function(idTemporada, idEtapa) {
        var vo = {
            idTemporada : this.currentEtapa.temporada.idTemporada,
            idEtapa : this.currentEtapa.idEtapa,
            idEtapaToCopy : idEtapa
        }
        
        var t = this;
        EtapaService.copyLargadas(vo, function(data) {
            t.saved = true;
        });
    }

    this.salvarLargadas = function(list) {
        var vo = {
            idEtapa : this.currentEtapa.idEtapa,
            idTemporada : this.currentEtapa.temporada.idTemporada,
            largadas  : list
        }

        var t = this;
        EtapaService.saveLargadas(vo, function(data) {
            t.saved = true;
        });
    }

    this.disableSaveLargada = function() {
        var result = false;

        if (this.categoriaList != false) {
            
            angular.forEach(this.categoriaList, function(value, key) {
                if (value.largada != null) {
                    if (value.largada.horarioLargada == null || value.largada.horarioLargada == "" || value.largada.tempoProva == null || value.largada.tempoProva == "") {
                        result = true;
                    }                    
                } else {
                    result = true;
                }
            });

        } else {
            result = true;
        }
        
        return result;
    }

    this.getVo = function (idTemporada, idEtapa, idCategoria) {
        var vo = {
            idTemporada : idTemporada,
            idEtapa     : idEtapa,
            idCategoria : idCategoria
        }
        return vo;
    }

    this.getEtapa = function (vo) {
        var t = this;
        if (vo != null) {
            EtapaService.getEtapaByTemporadaAndId(vo, function(data) {
                moment.locale(AppConfig.dateLocale);
                angular.forEach(data, function(etapa, key) {
                    t.currentEtapa = etapa;
                    t.currentEtapa.dataLong = moment(etapa.data).format(AppConfig.dateFormat.long);

                    EtapaService.getLargadasByEtapa(etapa, function(result) {
                        t.currentEtapa = result[0];

                        CategoriaService.listCategoriaByTemporadaChecked(vo.idTemporada, function(result) {
                            t.categoriaList = result;

                            angular.forEach(t.categoriaList, function(categoria, key) {
                                angular.forEach(t.currentEtapa.largadas, function(largada, key) {
                                    if (categoria.idCategoria == largada.idCategoria) {
                                        categoria.largada = largada;
                                    }
                                });
                            });
                        });
                        
                    });

                });
            });
        }
    }
    
    if ($routeParams != null) {
        if ($routeParams.idTemporada != null && $routeParams.idEtapa != null) {
            var idTemporada =$routeParams.idTemporada;
            var idEtapa = $routeParams.idEtapa; 
            var vo = this.getVo(idTemporada, idEtapa, "");

            this.getEtapa(vo);
        }
    }
}