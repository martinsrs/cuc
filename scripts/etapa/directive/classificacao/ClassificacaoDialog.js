'use strict';

angular.module('cucApp').directive('classificacaoDialog', ClassificacaoDialog);

function ClassificacaoDialog() {

	return {
		templateUrl: 'scripts/etapa/directive/classificacao/ClassificacaoDialog.html',
		restrict: 'E',
        scope : {
            dialogId : '@',
            idTemporada : '=',
            idEtapa     : '='
        },
		controller: ClassificacaoDialogCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

ClassificacaoDialogCtrl.$inject = ['$scope', 'AppConfig', 'AtletaService', 'EtapaService'];

function ClassificacaoDialogCtrl($scope, AppConfig, AtletaService, EtapaService) {

    $scope.listClassificacao = false;
    $scope.vo = {}
    
    $scope.currentEtapa = {};

    var t = this;
    $scope.$watch("idTemporada", function() {
        
        if ( $scope.idEtapa != null || $scope.idCategoria != null) {
            $scope.vo.idEtapa = null;
            $scope.vo.idCategoria = null;
        }

    });
    
    $scope.$watch("idEtapa", function() {
        $scope.idCategoria = "";
        $scope.currentEtapa = {};

        $scope.vo = t.getVo($scope.idTemporada, $scope.idEtapa, $scope.idCategoria);
        t.getEtapa($scope.vo);
        t.updateTable($scope.vo);
    });
    
    $scope.$watch("idCategoria", function() {
        if ($scope.idTemporada != null && $scope.idEtapa != null & $scope.idCategoria != null) {
            $scope.vo = t.getVo($scope.idTemporada, $scope.idEtapa, $scope.idCategoria);
            t.updateTable($scope.vo);
        }
    });
    
    $scope.disableSave = function() {
        
        var result = false;
        if ($scope.currentEtapa != null && $scope.currentEtapa.publicarResultado) {
            result = true;
        } else if ($scope.listClassificacao != false) {
            angular.forEach($scope.listClassificacao, function(value, key) {
               if (value.classificacao == null || value.classificacao == "" || value.classificacao <= 0) {
                   result = true;
               }
            });
        } else {
            result = true;
        }
        
        return result;
    }
    
    $scope.salvar = function() {
        AtletaService.saveClassificacao($scope.listClassificacao, function () {
            $scope.vo = t.getVo($scope.idTemporada, $scope.idEtapa, $scope.idCategoria);
            t.updateTable($scope.vo);
        });
    }

    this.getEtapa = function (vo) {
        if (vo != null) {
            EtapaService.getEtapaByTemporadaAndId(vo, function(data) {
                angular.forEach(data, function(value, key) {
                    $scope.currentEtapa = value;
                });
            });
        }
    }
    
    this.updateTable = function (vo) {        
        $scope.listClassificacao = false;
        
        AtletaService.listClassificacao(vo.idTemporada, vo.idEtapa, vo.idCategoria, function(data) {
            $scope.listClassificacao = data;
        });   
    }
    
    this.getVo = function (idTemporada, idEtapa, idCategoria) {
        var vo = {
            idTemporada : idTemporada,
            idEtapa     : idEtapa,
            idCategoria : idCategoria
        }
        return vo;
    }
    
    
}