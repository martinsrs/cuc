'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:EtapaForm
 * @description
 * # EtapaForm
 */
angular.module('cucApp').directive('etapaForm', EtapaForm);

function EtapaForm() {

	return {
		templateUrl: 'scripts/etapa/directive/EtapaForm.html',
		restrict: 'E',
        scope : {
            temporadaId : '=',
            etapa : '='
        },
		controller: EtapaFormCtrl,
		link: function (scope, elem, attr) {
            $('.datepicker').pickadate({format: 'dd/mm/yyyy', formatSubmit : 'yyyy-mm-dd'});
		}
    };
}

EtapaFormCtrl.$inject = ['$scope', 'AppConfig' ,'TemporadaService', 'EtapaService'];

function EtapaFormCtrl($scope, AppConfig, TemporadaService, EtapaService) {
    $scope.vo = {};
    
    
    $scope.$watch('temporadaId', function() {
        TemporadaService.getTemporadaById($scope.temporadaId, function(data){
            $scope.vo.temporada = data;
            $scope.vo.etapa = {};
        });
    });
    
    $scope.$watch('etapa', function() {
        $scope.vo.etapa = $scope.etapa;
          
        if ($scope.etapa != null && $scope.etapa != null && $scope.etapa.data != null) {
            var dateFormat = formatDate($scope.etapa.data, AppConfig.dateFormat.short);
            $scope.vo.etapa.data = dateFormat;
            
            var $input = $('.datepicker').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', dateFormat, { format: 'dd/mm/yyyy' });
        } else {
            var $input = $('.datepicker').pickadate();
            var picker = $input.pickadate('picker');
            picker.set('select', new Date(), { format: 'dd/mm/yyyy' });
            picker.clear();
        }
        
    });
    
    $scope.salvar = function (vo) {
        var etapaVo = vo;
        etapaVo.data = moment(etapaVo.data, AppConfig.dateFormat.short).format(AppConfig.dateFormat.default);
        etapaVo.temporada = $scope.vo.temporada;
        
        console.log($scope.vo.temporada);
        console.log(etapaVo);
        
        EtapaService.saveEtapa(etapaVo, function() {
            $('#formEtapaSubmit').submit();
        });
    }
}

function formatDate(date, outputFormat) {
    
    var formatted;
    var inputFormat = 'YYYY-MM-DD';
    
    if (date.indexOf('/') > 0) {
        inputFormat = 'DD/MM/YYYY';
    }
    
    return moment(date, inputFormat).format(outputFormat);
}