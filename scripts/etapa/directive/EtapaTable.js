'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:EtapaTable
 * @description
 * # EtapaTable
 */
angular.module('cucApp').directive('etapaTable', EtapaTable);

function EtapaTable() {

	return {
		templateUrl: 'scripts/etapa/directive/EtapaTable.html',
		restrict: 'E',
        scope : {
            
        },
		controller: EtapaTableCtrl,
		link: function (scope, elem, attr) {

		}
    };
}

EtapaTableCtrl.$inject = ['$scope', 'EtapaService', 'TemporadaService'];

function EtapaTableCtrl($scope, EtapaService, TemporadaService) {
    $scope.vo = {};
    $scope.temporadas = [];
    $scope.etapaList = [];
    
    TemporadaService.getTemporadaList(function(data){
        $scope.temporadas = data;
    });
    
    $scope.$watch('vo.idTemporada', function() {
        $scope.etapaList = [];
        
        EtapaService.getEtapaListByTemporada($scope.vo, function(data){
            $scope.etapaList = data;
        });
    });    

    $scope.publicar = function(vo) {
        $scope.turnEdit(null);
        EtapaService.publicarResultados(vo, function () {
            EtapaService.getEtapaListByTemporada($scope.vo, function(data){
                $scope.etapaList = data;
            });
        });
    }
    
    $scope.despublicar = function(vo) {
        $scope.turnEdit(null);
        EtapaService.despublicarResultados(vo, function () {
            EtapaService.getEtapaListByTemporada($scope.vo, function(data){
                $scope.etapaList = data;
            });
        });
    }
    
    $scope.prepareDel = function(vo) {
        $scope.toDeleteVo = vo;
    }
    
    $scope.turnEdit = function(vo) {
        $scope.toEditVo = vo;
    }
    
    $scope.delete = function(vo) {
        EtapaService.deleteEtapa(vo, function () {
            EtapaService.getEtapaListByTemporada($scope.vo, function(data){
                $scope.etapaList = data;
            });            
        });
    }
}