'use strict';

angular.module('cucApp').directive('etapaCombo', EtapaCombo);

function EtapaCombo() {

	return {
		template: '<select class="form-control" ng-model="ngModel" ng-disabled="ngDisabled">' +
                    '<option value="" disabled selected>Selecione uma etapa...</option>' +
                    '<option ng-repeat="vo in etapaList" ng-show="vo.showOption" value="{{vo.idEtapa}}">{{vo.data}} - {{vo.local}}</option>' +
                  '</select>',
		restrict: 'E',
        scope : {
            ngModel : '=',
            ngDisabled : '=',
            temporada : '=',
            idTemporada : '=',
            inscricao : '@',
            publicado : '@'
        },
		controller: EtapaComboCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

EtapaComboCtrl.$inject = ['$scope', 'AppConfig', 'EtapaService'];

function EtapaComboCtrl($scope, AppConfig, EtapaService) {
    
    var t = this;    
    $scope.etapaList = [];
    
    $scope.$watch('idTemporada', function() {
        
         if ($scope.idTemporada != null) {
            var vo = { idTemporada : $scope.idTemporada };
            
            if ($scope.inscricao) {
                EtapaService.getEtapaListByTemporadaInscricao(vo, function(data){        
                    angular.forEach(data, function (value, key) {
                        var dataFormat = moment(value.data).format(AppConfig.dateFormat.short);
                        value.data = dataFormat;
                        
                        if ($scope.publicado) {
                            value.showOption = value.publicarResultado;
                        } else {
                            value.showOption = true;
                        }
                    });
                    $scope.etapaList = data;
                });
            } else {
                EtapaService.getEtapaListByTemporada(vo, function(data){        
                    angular.forEach(data, function (value, key) {
                        var dataFormat = moment(value.data).format(AppConfig.dateFormat.short);
                        value.data = dataFormat;

                        if ($scope.publicado) {
                            value.showOption = value.publicarResultado;
                        } else {
                            value.showOption = true;
                        }
                    });
                    $scope.etapaList = data;
                });
            }
             
        }
    });
    
    $scope.$watch('temporada', function() {  
        
        if ($scope.temporada != null && $scope.temporada.idTemporada != null) {            
            
            if ($scope.inscricao) {
                EtapaService.getEtapaListByTemporadaInscricao($scope.temporada, function(data){        
                    angular.forEach(data, function (value, key) {
                        var dataFormat = moment(value.data).format(AppConfig.dateFormat.short);
                        value.data = dataFormat;
                        
                        if ($scope.publicado) {
                            value.showOption = value.publicarResultado;
                        } else {
                            value.showOption = true;
                        }
                    });
                    $scope.etapaList = data;
                });
            } else {
                EtapaService.getEtapaListByTemporada($scope.temporada, function(data){        
                    angular.forEach(data, function (value, key) {
                        var dataFormat = moment(value.data).format(AppConfig.dateFormat.short);
                        value.data = dataFormat;
                        
                        if ($scope.publicado) {
                            value.showOption = value.publicarResultado;
                        } else {
                            value.showOption = true;
                        }
                    });
                    $scope.etapaList = data;
                });                
            }
        }
    });
    
}