'use strict';

angular.module('cucApp').directive('atletaTable', AtletaTable);

function AtletaTable() {

	return {
		templateUrl: 'scripts/atleta/directive/AtletaTable.html',
		restrict: 'E',
        scope : {
            
        },
		controller: AtletaTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

AtletaTableCtrl.$inject = ['$scope', 'AtletaService'];

function AtletaTableCtrl($scope, AtletaService) {
    var t = this;
    
    AtletaService.listAtletas(function(data){
        $scope.atletaList = data;
        t.atletaList = data;
        console
    });
    
    $scope.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }
    
    $scope.save = function(vo) {     
        AtletaService.saveAtleta(vo, function () {
            vo.edit = false;
            AtletaService.listAtletas(function(data){
                $scope.atletaList = data;
                t.atletaList = data;
            });
        });
    }
        
    $scope.prepareDel = function(vo) {
        $scope.toDeleteVo = vo;
    }
    
    $scope.delete = function(vo) {
        AtletaService.deleteAtleta(vo, function () {
            
            AtletaService.listAtletas(function(data){
                $scope.atletaList = data;
                t.atletaList = data;
            });
            
        });
    }
}