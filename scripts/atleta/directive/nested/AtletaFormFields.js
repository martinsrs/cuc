'use strict';

angular.module('cucApp').directive('atletaFormFields', AtletaFormFields);

function AtletaFormFields() {

	return {
		templateUrl: 'scripts/atleta/directive/nested/AtletaFormFields.html',
		restrict: 'E',
        scope : {
            insertVo : '='
        },
		controller: AtletaFormFieldsCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

AtletaFormFieldsCtrl.$inject = ['$scope'];

function AtletaFormFieldsCtrl($scope) {
    $scope.insertVo = { equipe : null };
}