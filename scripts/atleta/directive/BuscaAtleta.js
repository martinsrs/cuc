'use strict';

angular.module('cucApp').directive('buscaAtleta', BuscaAtleta);

function BuscaAtleta() {

	return {
		templateUrl: 'scripts/atleta/directive/BuscaAtleta.html',
		restrict: 'E',
        scope : {
            ngModel : '=',
            searchValue : '=',
            formAction : '@'
        },
		controller: BuscaAtletaCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

BuscaAtletaCtrl.$inject = ['$scope', 'AtletaService'];

function BuscaAtletaCtrl($scope, AtletaService) {
    
    $scope.atletaList = false;
        
    $scope.$watch('searchValue', function() {
        var query = $scope.searchValue;
        $scope.atletaList = false;
        $scope.ngModel = false;
        if (query != null && query.length >= 3) {
            
            AtletaService.getAtletaByQuery(query, function(data) {
                $scope.atletaList = data;
            });
        }
    });    
    
    $scope.desmarcaAtleta = function(callback) {
        $scope.ngModel = false; 
        $scope.atletaList = false;
        $scope.searchValue = "";
    };
    
    $scope.selecionaAtleta = function(vo, callback) {
        $scope.ngModel = vo; 
        $scope.atletaList = false;
    };
}