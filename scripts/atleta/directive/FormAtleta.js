'use strict';

angular.module('cucApp').directive('formAtleta', FormAtleta);

function FormAtleta() {

	return {
		templateUrl: 'scripts/atleta/directive/FormAtleta.html',
		restrict: 'E',
        scope : {
            formAction : '@',
            modal : '@'
        },
		controller: FormAtletaCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

FormAtletaCtrl.$inject = ['$scope', 'AtletaService'];

function FormAtletaCtrl($scope, AtletaService) {
    $scope.insertVo = { equipe : null };
    
    $scope.disableSave = function() {
        if ($scope.insertVo != null) {
            if ($scope.insertVo.nome != null 
               && $scope.insertVo.cidade != null 
               && $scope.insertVo.estado != null 
               && $scope.insertVo.equipe != null 
               && $scope.insertVo.equipe.idEquipe != null) {
                return false;
            }
        }
            
        return true;
    }
    
    $scope.saveAtleta = function (vo) {
        AtletaService.saveAtleta(vo, function() {
        });
    }
}