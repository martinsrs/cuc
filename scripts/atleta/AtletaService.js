'use strict';


angular.module('cucApp').service('AtletaService', AtletaService);

AtletaService.$inject = ['$http', 'AppConfig'];

function AtletaService($http, AppConfig) {
    
    this.listAtletas = function (callback) {
        
        $http.get("core/atleta/atleta.php", {}).then(function(result) {;
            if (result.data != null && result.data.length > 0) {
                var listAtletas = result.data;
                return callback(listAtletas);
            }
        });
    }
    
    this.getAtletaByQuery = function(query, callback) {
        $http.get("core/atleta/atleta.php?query="+query, {}).then(function(result) {;
            if (result.data != null && result.data.length > 0) {
                var listAtletas = result.data;            
                return callback(listAtletas);
            }
        });
    }
        
    this.deleteAtleta = function (vo, callback) {
        $http.post("core/atleta/deleteAtleta.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.saveAtleta = function (vo, callback) {
        $http.post("core/atleta/saveAtleta.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.inscreverAtleta = function (vo, callback) {
        $http.post("core/atleta/inscrever.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.inscreverAtleta = function (vo, callback) {
        $http.post("core/atleta/inscrever.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.listInscricaoAtleta = function (idAtleta, idEtapa, callback) {
        
        $http.get("core/atleta/atleta.php?inscricao=1&id_atleta=" + idAtleta + "&id_etapa=" + idEtapa, {}).then(function(result) {;
            if (result.data != null && result.data.length > 0) {
                var listAtletas = result.data;            
                return callback(listAtletas);
            }
        });
    }
    
    this.listClassificacao = function (idTemporada, idEtapa, idCategoria, callback) {
        
        var query = "?id_temporada="+ idTemporada +"&id_etapa="+ idEtapa +"&id_categoria=" + idCategoria

        $http.get("core/atleta/classificacao.php" + query, {}).then(function(result) {;
            if (result.data != null && result.data.length > 0) {
                var listAtletas = result.data;            
                return callback(listAtletas);
            }
        });
    }

    this.saveClassificacao = function (vo, callback) {
        $http.post("core/atleta/saveClassificacao.php", vo).then(function(result) {
            return callback();
        });
    }
    
}