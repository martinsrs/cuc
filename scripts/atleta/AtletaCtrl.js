'use strict';

angular.module('cucApp')
  .controller('AtletaCtrl', AtletaCtrl);

AtletaCtrl.$inject = ['$scope', '$route', 'AtletaService'];

function AtletaCtrl($scope, $route, AtletaService) {

    this.insertVo = { equipe : null };
    
    this.disableSave = function() {
        if (this.insertVo != null) {
            if (this.insertVo.nome != null && this.insertVo.cidade != null && this.insertVo.estado != null && this.insertVo.equipe != null && this.insertVo.equipe.idEquipe != null) {
                return false;
            }
        }
            
        return true;
    }
    
    this.saveAtleta = function (vo) {
        AtletaService.saveAtleta(vo, function(){
        });
    }
}