'use strict';

/**
 * @ngdoc function
 * @name cucApp.controller:EquipeCtrl
 * @description
 * # EquipeCtrl
 * Controller of the cucApp
 */ 
angular.module('cucApp')
  .controller('EquipeCtrl', EquipeCtrl);

EquipeCtrl.$inject = ['$scope', '$route', 'AppConfig', 'EquipeService'];

function EquipeCtrl($scope, $route, AppConfig, EquipeService) {

    this.insertVo = {};
    
    this.disableSave = function() {
        if (this.insertVo != null) {
            if (this.insertVo.nome != null && this.insertVo.cidade != null && this.insertVo.estado != null) {
                return false;
            }
        }
            
        return true;
    }
    
    this.saveEquipe = function (vo) {
        EquipeService.saveEquipe(vo, function(){
        });
    }
}