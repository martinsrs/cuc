'use strict';


angular.module('cucApp').service('EquipeService', EquipeService);

EquipeService.$inject = ['$http', 'AppConfig'];

function EquipeService($http, AppConfig) {
    
    this.listEquipes = function (callback) {
        
        $http.get("core/equipe/equipe.php", {}).then(function(result) {
            
            if (result.data != null && result.data.length > 0) {
                
                var listEquipes = [];
                
                for(var i=0; i<result.data.length; i++) {
                    
                    var vo = {
                        idEquipe : result.data[i].idEquipe,
                        nome : result.data[i].nome,
                        cidade  : result.data[i].cidade,
                        estado  : result.data[i].estado
                    }
                    
                    listEquipes.push(vo);
                }
            
                return callback(listEquipes);
            }
            
        });
    }
        
    this.deleteEquipe = function (vo, callback) {
        $http.post("core/equipe/deleteEquipe.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.saveEquipe = function (vo, callback) {
        console.log(vo);
        $http.post("core/equipe/saveEquipe.php", vo).then(function(result) {
            return callback();
        });
    }
    
}