'use strict';

angular.module('cucApp').directive('equipeTable', EquipeTable);

function EquipeTable() {

	return {
		templateUrl: 'scripts/equipe/directive/EquipeTable.html',
		restrict: 'E',
        scope : {
            
        },
		controller: EquipeTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

EquipeTableCtrl.$inject = ['$scope', 'EquipeService'];

function EquipeTableCtrl($scope, EquipeService) {
    var t = this;
    
    EquipeService.listEquipes(function(data){
        $scope.equipeList = data;
        t.equipeList = data;
    });
    
    $scope.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }
    
    $scope.save = function(vo) {     
        EquipeService.saveEquipe(vo, function () {
            vo.edit = false;
            EquipeService.listEquipes(function(data){
                $scope.equipeList = data;
                t.equipeList = data;
            });
        });
    }
        
    $scope.prepareDel = function(vo) {
        $scope.toDeleteVo = vo;
    }
    
    $scope.delete = function(vo) {
        EquipeService.deleteEquipe(vo, function () {
            
            EquipeService.listEquipes(function(data){
                $scope.equipeList = data;
                t.equipeList = data;
            });
            
        });
    }
}