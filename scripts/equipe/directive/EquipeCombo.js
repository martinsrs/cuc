'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:EquipeCombo
 * @description
 * # EquipeCombo
 */
angular.module('cucApp').directive('equipeCombo', EquipeCombo);

function EquipeCombo() {
	return {
		template: '<select class="form-control" ng-model="ngModel">' +
                      '<option value="" disabled selected>Selecione uma Equipe...</option>' + 
                      '<option ng-repeat="equipe in equipes" value="{{equipe.idEquipe}}">{{equipe.nome}}</option>' +
                  '</select>',
		restrict: 'E',
        scope : {
            ngModel : '='
        },
		controller: EquipeComboCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

EquipeComboCtrl.$inject = ['$scope', 'EquipeService'];

function EquipeComboCtrl($scope, EquipeService ) {
    $scope.vo = {};
    
    EquipeService.listEquipes(function(data){
        $scope.equipes = data;
    });
}