'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:EquipeForm
 * @description
 * # EquipeForm
 */
angular.module('cucApp').directive('equipeForm', EquipeForm);

function EquipeForm() {

	return {
		templateUrl: 'scripts/equipe/directive/EquipeForm.html',
		restrict: 'E',
        scope : {
            formAction : '@'
        },
		controller: EquipeFormCtrl,
		link: function (scope, elem, attr) {
		}
    };
}

EquipeFormCtrl.$inject = ['$scope', 'AppConfig', 'EquipeService'];

function EquipeFormCtrl($scope, AppConfig, EquipeService) {
    $scope.vo = {};

    $scope.saveEquipe = function (vo) {
        EquipeService.saveEquipe(vo, function() {
            $('#formDialogEquipe').submit();
        });
    }
}