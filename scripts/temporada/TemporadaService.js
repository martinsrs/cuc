'use strict';

/**
 * @ngdoc function
 * @name cucApp.service:TemporadaService
 * @description
 * # TemporadaService
 */
angular.module('cucApp').service('TemporadaService', TemporadaService);

TemporadaService.$inject = ['$http', 'AppConfig'];

function TemporadaService($http, AppConfig) {
    
    this.getTemporadaList = function (callback) {
        
        $http.get("core/temporada/temporada.php", {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
               
                var temporadaList = [];
                for(var i=0; i<result.data.length; i++) {
                    
                    var temporadaVo = {
                        idTemporada : result.data[i].id_temporada,
                        ano : result.data[i].ano,
                        edicao  : result.data[i].edicao,
                        publicado  : result.data[i].publicado
                    }
                    temporadaList.push(temporadaVo);
                }
                
                return callback(temporadaList);
            }
        });    
    }
    
    this.getAllTemporadaList = function (callback) {
        
        $http.get("core/temporada/temporada.php?all=true", {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
               
                var temporadaList = [];
                for(var i=0; i<result.data.length; i++) {
                    
                    var temporadaVo = {
                        idTemporada : result.data[i].id_temporada,
                        ano : result.data[i].ano,
                        edicao  : result.data[i].edicao,
                        publicado  : result.data[i].publicado
                    }
                    temporadaList.push(temporadaVo);
                }
            
                return callback(temporadaList);
            }
        });    
    }
    
    this.getTemporadaById = function (id, callback) {
        
        $http.get("core/temporada/temporada.php?id_temporada=" + id, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
               
                var temporadaVo;
                for(var i=0; i<result.data.length; i++) {
                    
                    temporadaVo = {
                        idTemporada : result.data[i].id_temporada,
                        ano : result.data[i].ano,
                        edicao  : result.data[i].edicao
                    }
                }
                
                return callback(temporadaVo);
            }
        });    
    }

    this.getTemporadaEtapa = function (filter, callback) {

        var paginateFilter = "";
        if (filter != null && filter.paginate && filter.pageSize != null && filter.currentPage != null) {
            if (filter.paginate == "true") {
                paginateFilter = "&pageSize="+filter.pageSize+"&currentPage=" + filter.currentPage;
            }
        }

        $http.get("core/temporada/temporada.php?etapa=true" + paginateFilter, {} ).then(function(result) {

            //console.log(result);

            if (result.data != null && result.data.list.length > 0) {


                return callback(result.data);
            }
        });
    }

    this.getOverallRanking = function (idTemporada, idCategoria, callback) {
        $http.get("core/temporada/ranking.php?id_temporada="+idTemporada+"&id_categoria=" + idCategoria, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            }
        });
    }

    this.getOverallTeamRanking = function (idTemporada, callback) {
        $http.get("core/temporada/ranking.php?id_temporada="+idTemporada+"&team=true", {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            }
        });
    }

    this.getEtapaRanking = function (idTemporada, idCategoria, idEtapa, callback) {
        $http.get("core/temporada/ranking.php?id_temporada="+idTemporada+"&id_categoria=" + idCategoria + "&id_etapa=" + idEtapa, {} ).then(function(result) {
            if (result.data != null && result.data.length > 0) {
                return callback(result.data);
            }
        });
    }
        
    this.assignCategorias = function (vo, callback) {
        $http.post("core/temporada/assignCategorias.php", vo).then(function(result) {
            return callback();
        });
    }
    
    this.deleteTemporada = function (temporadaVo, callback) {
        $http.post("core/temporada/deleteTemporada.php", temporadaVo).then(function(result) {
            return callback();
        });
    }
    
    this.saveTemporada = function (temporadaVo, callback) {
        $http.post("core/temporada/saveTemporada.php", temporadaVo).then(function(result) {
            return callback();
        });
    }
    
}