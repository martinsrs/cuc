'use strict';

angular.module('cucApp').directive('rankingTable', RankingTable);

function RankingTable() {

	return {
		templateUrl: 'scripts/temporada/ranking/RankingTable.html',
		restrict: 'E',
        scope : {

        	idTemporada : '=',
        	idCategoria : '=',
            hideHeader  : '@'
            
        },
		controller: RankingTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

RankingTableCtrl.$inject = ['$scope', '$window', 'TemporadaService'];

function RankingTableCtrl($scope, $window, TemporadaService) {

	$scope.rankingList = false;

    $scope.$watch("idCategoria", function() {
    	$scope.rankingList = false;

        TemporadaService.getOverallRanking($scope.idTemporada, $scope.idCategoria, function(data) {
			$scope.rankingList = data;        	
        });

    });

}