'use strict';

angular.module('cucApp').directive('rankingEtapaTable', RankingEtapaTable);

function RankingEtapaTable() {

	return {
		templateUrl: 'scripts/temporada/ranking/RankingEtapaTable.html',
		restrict: 'E',
        scope : {

        	idTemporada : '=',
        	idCategoria : '=',
            idEtapa     : '=',
            hideHeader  : '@'
            
        },
		controller: RankingEtapaTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

RankingEtapaTableCtrl.$inject = ['$scope', '$window', 'TemporadaService'];

function RankingEtapaTableCtrl($scope, $window, TemporadaService) {

	$scope.rankingList = false;

    $scope.$watch("idCategoria", function() {
    	$scope.rankingList = false;

        TemporadaService.getEtapaRanking($scope.idTemporada, $scope.idCategoria, $scope.idEtapa, function(data) {
            $scope.rankingList = data;        	
        });

    });

    $scope.$watch("idEtapa", function() {
        $scope.idCategoria = null;
    });

}