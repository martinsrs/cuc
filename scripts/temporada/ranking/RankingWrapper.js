'use strict';

angular.module('cucApp').directive('rankingWrapper', RankingWrapper);

function RankingWrapper() {

	return {
		templateUrl: 'scripts/temporada/ranking/RankingWrapper.html',
		restrict: 'E',
        scope : {

        	idTemporada : '=',
            idEtapa     : '=', 
        	idCategoria : '='
            
        },
		controller: RankingWrapperCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

RankingWrapperCtrl.$inject = ['$scope', 'CategoriaService'];

function RankingWrapperCtrl($scope, CategoriaService) {

	$scope.categoriaList = false;
    $scope.filter = {};

    $scope.$watch("idTemporada", function() {
    	$scope.filter = {
            idTemporada : $scope.idTemporada
        }

        $scope.categoriaList = false;
        CategoriaService.getAllCategoriaWithResultList($scope.filter, function(data) {
			$scope.categoriaList = data;        	
        });
    });

    $scope.$watch("idCategoria", function() {
        $scope.filter.idCategoria = $scope.idCategoria;

        $scope.categoriaList = false;
        CategoriaService.getAllCategoriaWithResultList($scope.filter, function(data) {
            $scope.categoriaList = data;            
        });
    });

}