'use strict';

angular.module('cucApp').directive('rankingWrapperEtapa', RankingWrapperEtapa);

function RankingWrapperEtapa() {

	return {
		templateUrl: 'scripts/temporada/ranking/RankingWrapperEtapa.html',
		restrict: 'E',
        scope : {

        	idTemporada : '=',
            idEtapa     : '=', 
        	idCategoria : '='
            
        },
		controller: RankingWrapperEtapaCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

RankingWrapperEtapaCtrl.$inject = ['$scope', 'CategoriaService', 'EtapaService'];

function RankingWrapperEtapaCtrl($scope, CategoriaService, EtapaService) {

	$scope.categoriaList = false;
    $scope.etapa = null;
    $scope.filter = {};

    $scope.$watch("idTemporada", function() {
    	$scope.filter = {
            idTemporada : $scope.idTemporada
        }

        $scope.categoriaList = false;
        CategoriaService.getAllCategoriaWithResultList($scope.filter, function(data) {
			$scope.categoriaList = data;        	
        });
    });

    $scope.$watch("idEtapa", function() {
        $scope.filter.idEtapa = $scope.idEtapa;
        $scope.categoriaList = false;
        $scope.etapa = null;

        EtapaService.getEtapaByTemporadaAndId($scope.filter, function(data){
            if (data != null && data.length > 0) {
                $scope.etapa = data[0];
            }
        });

        CategoriaService.getAllCategoriaWithResultList($scope.filter, function(data) {
            $scope.categoriaList = data;            
        });
    });

    $scope.$watch("idCategoria", function() {
        $scope.filter.idCategoria = $scope.idCategoria;

        $scope.categoriaList = false;
        CategoriaService.getAllCategoriaWithResultList($scope.filter, function(data) {
            $scope.categoriaList = data;            
        });
    });

}