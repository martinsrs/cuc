'use strict';

angular.module('cucApp').directive('rankingTeamTable', RankingTeamTable);

function RankingTeamTable() {

	return {
		templateUrl: 'scripts/temporada/ranking/RankingTeamTable.html',
		restrict: 'E',
        scope : {

        	idTemporada : '='
            
        },
		controller: RankingTeamTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

RankingTeamTableCtrl.$inject = ['$scope', '$window', 'TemporadaService'];

function RankingTeamTableCtrl($scope, $window, TemporadaService) {

	$scope.rankingList = false;

    $scope.$watch("idTemporada", function() {
        $scope.rankingList = false;

        TemporadaService.getOverallTeamRanking($scope.idTemporada, function(data) {
            $scope.rankingList = data;          
        });

    });

}