'use strict';

/**
 * @ngdoc function
 * @name cucApp.controller:TemporadaCtrl
 * @description
 * # TemporadaCtrl
 * Controller of the cucApp
 */ 
angular.module('cucApp')
  .controller('TemporadaCtrl', TemporadaCtrl);

TemporadaCtrl.$inject = ['$scope', '$route', '$window', 'AppConfig', 'TemporadaService'];

function TemporadaCtrl($scope, $route, $window, AppConfig, TemporadaService) {

    this.idTemporada = '';
    this.temporadaVo;
    
    var t = this;
    TemporadaService.getTemporadaList(function(data){
        t.temporadaList = data;
    });
    
    TemporadaService.getAllTemporadaList(function(data){
        t.temporadaAllList = data;
    });
    
    //get temporada by id
    if ($route.current != null) {
        if ($route.current.params.id != null) {
            this.idTemporada = $route.current.params.id;    

            TemporadaService.getTemporadaById(this.idTemporada, function(data) {
                t.temporadaVo = data;
            });
        }    
    }
    
    this.disableSave = function() {
        if (this.temporadaVo != null) {
            if (this.temporadaVo.ano != null && this.temporadaVo.edicao != null) {
                return false;
            }
        }
            
        return true;
    }
    
    this.list = function () {
        var t = this;
        TemporadaService.getTemporadaList(function(data){
            t.temporadaList = data;
        });
    }
    
    this.listAll = function () {
        var t = this;
        TemporadaService.getAllTemporadaList(function(data){
            t.temporadaAllList = data;
        });
    }
    
    this.saveTemporada = function (temporadaVo) {
        TemporadaService.saveTemporada(temporadaVo, function(){
            
        });
    }
}