'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:TemporadaTable
 * @description
 * # TemporadaTable
 */
angular.module('cucApp').directive('temporadaTable', TemporadaTable);

function TemporadaTable() {

	return {
		templateUrl: 'scripts/temporada/directive/TemporadaTable.html',
		restrict: 'E',
        scope : {
            
        },
		controller: TemporadaTableCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

TemporadaTableCtrl.$inject = ['$scope', '$window', 'TemporadaService'];

function TemporadaTableCtrl($scope, $window, TemporadaService) {
    var t = this;
    
    TemporadaService.getAllTemporadaList(function(data){
        $scope.temporadaAllList = data;
        t.temporadaAllList = data;
    });
    
    $scope.turnEdit = function(vo) {
        if (vo.edit == null || vo.edit == false) {
            vo.edit = true;
        } else {
            vo.edit = false;
        }
        return vo;
    }
    
    $scope.save = function(vo) {     
        TemporadaService.saveTemporada(vo, function () {
            vo.edit = false;
        });
    }
    
    $scope.popUp = function(vo) {
        $scope.popUpVo = vo;
    }
    
    $scope.prepareDel = function(vo) {
        $scope.toDeleteVo = vo;
    }
    
    $scope.delete = function(vo) {
        TemporadaService.deleteTemporada(vo, function () {
            TemporadaService.getAllTemporadaList(function(data){
                $scope.temporadaAllList = data;
                t.temporadaAllList = data;
            });
        });
    }
    
    $scope.publicar = function(vo) {
        vo.publicado = 1;
        TemporadaService.saveTemporada(vo, function () {
        });
    }
    
    $scope.despublicar = function(vo) {
        vo.publicado = 0;        
        TemporadaService.saveTemporada(vo, function () {
        });
    }
}