'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:TemporadaCombo
 * @description
 * # TemporadaCombo
 */
angular.module('cucApp').directive('temporadaCombo', TemporadaCombo);

function TemporadaCombo() {
	return {
		template: '<select class="form-control" ng-model="ngModel" ng-disabled="ngDisabled">' +
                      '<option value="" disabled selected>Selecione uma temporada...</option>' + 
                      '<option ng-repeat="temporada in temporadas" value="{{temporada.idTemporada}}">{{temporada.ano}} - {{temporada.edicao}}</option>' +
                  '</select>',
		restrict: 'E',
        scope : {
            ngModel : '=',
            ngDisabled : '='
        },
		controller: TemporadaComboCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

TemporadaComboCtrl.$inject = ['$scope', 'TemporadaService'];

function TemporadaComboCtrl($scope, TemporadaService) {
    $scope.vo = {};
    $scope.temporadas = [];
    
    TemporadaService.getTemporadaList(function(data){
        $scope.temporadas = data;
    });
}