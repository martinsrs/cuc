'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:FormatDate
 * @description
 * # FormatDate
 */
angular.module('cucApp').directive('switch', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).bootstrapSwitch()
        }
    };
});