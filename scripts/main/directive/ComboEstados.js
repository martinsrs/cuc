'use strict';

/**
 * @ngdoc directive
 * @name cucApp.directive:ComboEstados
 * @description
 * # ComboEstados
 */
angular.module('cucApp').directive('comboEstados', ComboEstados);

function ComboEstados() {

	return {
		template: '<select class="form-control" ng-model="ngModel">' +
                      '<option value="" disabled selected>Selecione um Estado...</option>' + 
                      '<option ng-repeat="estado in estados" value="{{estado.sigla}}">{{estado.nome}}</option>' +
                  '</select>',
		restrict: 'E',
        scope : {
            ngModel : '='
        },
		controller: ComboEstadosCtrl,
		link: function (scope, elem, attr) {
            
		}
    };
}

ComboEstadosCtrl.$inject = ['$scope', 'AppConfig'];

function ComboEstadosCtrl($scope, AppConfig) {
    $scope.estados = [];
    $scope.estados.push({ nome: 'Acre', sigla: 'AC' });
    $scope.estados.push({ nome: 'Alagoas', sigla: 'AL' });
    $scope.estados.push({ nome: 'Amapá', sigla: 'AP' });
    $scope.estados.push({ nome: 'Amazonas', sigla: 'AM' });
    $scope.estados.push({ nome: 'Bahia', sigla: 'BA' });
    $scope.estados.push({ nome: 'Ceará', sigla: 'CE' });
    $scope.estados.push({ nome: 'Distrito Federal', sigla: 'DF' });
    $scope.estados.push({ nome: 'Espírito Santo', sigla: 'ES' });
    $scope.estados.push({ nome: 'Goiás', sigla: 'GO' });
    $scope.estados.push({ nome: 'Maranhão', sigla: 'MA' });
    $scope.estados.push({ nome: 'Mato Grosso', sigla: 'MT' });
    $scope.estados.push({ nome: 'Mato Grosso do Sul', sigla: 'MS' });
    $scope.estados.push({ nome: 'Minas Gerais', sigla: 'MG' });
    $scope.estados.push({ nome: 'Pará', sigla: 'PA' });
    $scope.estados.push({ nome: 'Paraíba', sigla: 'PB' });
    $scope.estados.push({ nome: 'Paraná', sigla: 'PR' });
    $scope.estados.push({ nome: 'Pernambuco', sigla: 'PE' });
    $scope.estados.push({ nome: 'Piauí', sigla: 'PI' });
    $scope.estados.push({ nome: 'Rio de Janeiro', sigla: 'RJ' });
    $scope.estados.push({ nome: 'Rio Grande do Norte', sigla: 'RN' });
    $scope.estados.push({ nome: 'Rio Grande do Sul', sigla: 'RS' });
    $scope.estados.push({ nome: 'Rondônia', sigla: 'RO' });
    $scope.estados.push({ nome: 'Roraima', sigla: 'RR' });
    $scope.estados.push({ nome: 'Santa Catarina', sigla: 'SC' });
    $scope.estados.push({ nome: 'São Paulo', sigla: 'SP' });
    $scope.estados.push({ nome: 'Sergipe', sigla: 'SE' });
    $scope.estados.push({ nome: 'Tocantins', sigla: 'TO' });
}