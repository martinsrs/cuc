<?php
require_once('EtapaVO.class.php');
require_once('EtapaDao.class.php');

$dao = new EtapaDao();
$vo = new EtapaVO();

if (isset($_GET['id_temporada']) && isset($_GET['id_etapa'])) {
    $vo->idEtapa = $_GET['id_etapa'];
    $vo->idTemporada = $_GET['id_temporada'];

	$result = $dao->getLargadasByEtapa($vo);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>