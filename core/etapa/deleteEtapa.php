<?php

require_once('EtapaVO.class.php');
require_once('EtapaDao.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $vo = $request;
    
    $dao = new EtapaDao();
    
    if (isset($vo->temporada)) {
        $result = $dao->deleteEtapa($vo);
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>