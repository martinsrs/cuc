<?php
require_once('../db.inc.php');
require_once('../temporada/TemporadaVO.class.php');

class EtapaVO {
    
    public $idEtapa;
    public $data;
    public $local;
    public $descricao;
    public $publicarResultado;
    
    public $temporada;
    public $largadas;

    private function addLargada($largada) {
        if ($this->largadas == null) {
            $this->largadas = array();
        }
        array_push($this->largadas, $largada);
    }
    
    public function loadFromResultSet($rs) {
        
        if (isset($rs['id_etapa'])) { $this->idEtapa = $rs['id_etapa']; }
        if (isset($rs['data'])) { $this->data = $rs['data']; }
        if (isset($rs['local'])) { $this->local = $rs['local']; }
        if (isset($rs['descricao'])) { $this->descricao = $rs['descricao']; }
        if (isset($rs['publicar_resultado'])) { 
            $this->publicarResultado = ($rs['publicar_resultado'] == 1) ? true : false ; 
        }
        
        if (isset($rs['id_temporada'])) {
            $this->temporada = new TemporadaVO();
            if (isset($rs['id_temporada'])) { $this->temporada->idTemporada = $rs['id_temporada']; }
            if (isset($rs['ano'])) { $this->temporada->ano = $rs['ano']; }
            if (isset($rs['edicao'])) { $this->temporada->edicao = $rs['edicao']; }
            if (isset($rs['publicado'])) { $this->temporada->publicado = ($rs['publicado'] == 1) ? true : false ; }
        }
    }

    public function loadFromResultSetNoChild($rs) {
        
        if (isset($rs['id_etapa'])) { $this->idEtapa = $rs['id_etapa']; }
        if (isset($rs['data'])) { $this->data = $rs['data']; }
        if (isset($rs['local'])) { $this->local = $rs['local']; }
        if (isset($rs['descricao'])) { $this->descricao = $rs['descricao']; }
        if (isset($rs['publicar_resultado'])) { 
            $this->publicarResultado = ($rs['publicar_resultado'] == 1) ? true : false ; 
        }
    }

    public function loadLargadasfromRS($rs) {
        $vo = new LargadaVO();
        $vo->loadLargadasfromRS($rs);
        $this->addLargada($vo);
    }
}
?>