<?php
require_once('EtapaVO.class.php');
require_once('EtapaDao.class.php');

$dao = new EtapaDao();
$vo = new EtapaVO();

if (isset($_GET['id_temporada'])) {
    $vo->idTemporada = $_GET['id_temporada'];

    if (isset($_GET['id_etapa'])) {
		$vo->idEtapa = $_GET['id_etapa'];
    	$result = $dao->getEtapaByTemporadaAndId($vo);
    } else if (isset($_GET['inscricao'])) {
        $result = $dao->listEtapaByTemporadaInscricao($vo);
    } else {
        $result = $dao->listEtapaByTemporada($vo);
    }

} else {
    $result = $dao->listEtapa();    
} 

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>