<?php
require_once('../db.inc.php');
require_once('EtapaVO.class.php');
require_once('LargadaVO.class.php');

class EtapaDao {
    
    private $SELECT_ETAPA = "select e.id_etapa, e.data, e.local, e.descricao, e.id_temporada, e.publicar_resultado, t.ano, t.edicao, t.publicado from etapa e join temporada t on (t.id_temporada = e.id_temporada) order by e.data desc";
    private $SELECT_ETAPA_INSCRICAO = "select e.id_etapa, e.data, e.local, e.descricao, e.id_temporada, e.publicar_resultado, t.ano, t.edicao, t.publicado from etapa e join temporada t on (t.id_temporada = e.id_temporada) where e.publicar_resultado=0 and e.id_temporada = ? order by e.data desc";
    private $SELECT_ETAPA_BY_TEMPORADA = "select e.id_etapa, e.data, e.local, e.descricao, e.id_temporada, e.publicar_resultado, t.ano, t.edicao, t.publicado from etapa e join temporada t on (t.id_temporada = e.id_temporada) where t.id_temporada = ? order by e.data desc";
    private $SELECT_ETAPA_BY_TEMPORADA_AND_ID = "select e.id_etapa, e.data, e.local, e.descricao, e.id_temporada, e.publicar_resultado, t.ano, t.edicao, t.publicado from etapa e join temporada t on (t.id_temporada = e.id_temporada) where t.id_temporada = ? and e.id_etapa = ? order by e.data desc";
    private $INSERT_ETAPA = "insert into etapa (data, local, descricao, id_temporada) values (?,?,?,?)";
    private $UPDATE_ETAPA = "update etapa set data=?, local=?, descricao=?, publicar_resultado=? where id_etapa = ?";  
    private $DELETE_ETAPA = "delete from etapa where id_etapa = ?";

    private $LIST_LARGADA_BY_ETAPA = "select el.id_etapa, el.id_categoria, el.id_temporada, el.horario_largada, el.tempo_prova,
                                            c.nome, c.criterio, c.valor_inscricao, c.ordem
                                       from etapa_largada el
                                       join categoria c on (c.id_categoria = el.id_categoria)
                                       where id_temporada = ? and id_etapa = ?
                                       and el.tempo_prova <> '--'
                                       order by el.horario_largada, c.ordem";
        
    private $SAVE_LARGADA = "insert into etapa_largada (id_etapa, id_temporada, id_categoria, horario_largada, tempo_prova) values (?,?,?,?,?)";
    private $DELETE_LARGADA = "delete from etapa_largada where id_etapa=? and id_temporada=?";

    private $COPY_LARGADAS_FROM_ETAPA = "insert into etapa_largada
                                            select ?, id_categoria, id_temporada, horario_largada, tempo_prova
                                            from etapa_largada
                                            where id_etapa = ? and id_temporada = ?";

    private $GET_COUNT_LARGADAS = "select count(*) as cnt
                                    from etapa_largada
                                    where id_etapa = ? and id_temporada = ? ";


    private function getEtapaListFromRS($rs) {
        
        $list = array();        
        foreach ($rs as $val) {
            $etapa = new EtapaVO();
            $etapa->loadFromResultSet($val);
            array_push($list, $etapa);
        }
        
        return $list;
    }

    private function getLargadaListFromRS($rs, $etapas) {

        foreach ($etapas as $etapa) {
            $listLargadas = array();
            foreach ($rs as $val) {
                if ($etapa->idEtapa == $val['id_etapa']) {
                    $largada = new LargadaVO();
                    $largada->loadFromResultSet($val);
                    $largada->categoria = new CategoriaVO();
                    $largada->categoria->loadFromResultSet($val);

                    array_push($listLargadas, $largada);
                }
            }

            $etapa->largadas = $listLargadas;
        }

        return $etapas;   
    }
    
    /**
        Lista todas Etapas
    */
    public function listEtapa() {
        $result = Db::executeQuery($this->SELECT_ETAPA, null);
        return $this->getEtapaListFromRS($result);
    }
    
    /**
        Lista todas Etapas da Temporada
    */
    public function listEtapaByTemporada($vo) {
        
        if (isset($vo->idTemporada)) {            
            $params = array();
            array_push($params, $vo->idTemporada);
            $result = Db::executeQuery($this->SELECT_ETAPA_BY_TEMPORADA, $params);
            return $this->getEtapaListFromRS($result);
        }
        
        return false;
    }

    public function getLargadasByEtapa($vo) {


        if (isset($vo->idTemporada) && isset($vo->idEtapa)) {
            $params = array();
            array_push($params, $vo->idTemporada);
            array_push($params, $vo->idEtapa);
            
            $rsEtapas = Db::executeQuery($this->SELECT_ETAPA_BY_TEMPORADA_AND_ID, $params);
            $etapas = $this->getEtapaListFromRS($rsEtapas);
            $largadas = Db::executeQuery($this->LIST_LARGADA_BY_ETAPA, $params);

            return $this->getLargadaListFromRS($largadas, $etapas);

        }
        
        return false;
    }

    /**
        Busca Etapa by ID
     */
    public function getEtapaByTemporadaAndId($vo) {

        if (isset($vo->idTemporada)) {
            $result = Db::executeQuery($this->SELECT_ETAPA_BY_TEMPORADA_AND_ID, array($vo->idTemporada, $vo->idEtapa));

            return $this->getEtapaListFromRS($result);
        }

        return false;
    }

    /**
    Lista todas Etapas para Inscrição
     */
    public function listEtapaByTemporadaInscricao($vo) {

        if (isset($vo->idTemporada)) {
            $result = Db::executeQuery($this->SELECT_ETAPA_INSCRICAO, array($vo->idTemporada));
            return $this->getEtapaListFromRS($result);
        }

        return false;
    }
    
    /**
        Exclui a etapa
    */
    public function deleteEtapa($vo) {
        $query = $this->DELETE_ETAPA;
        
        if (isset($vo)) {
            
            if (isset($vo)) {
                $params = array($vo->idEtapa);
                return Db::executeSave($query, $params);
            }
        }
        
        return false;
    }

    public function copyLargadasFromEtapa($vo) {
        
        $result = false;

        if (isset($vo) && isset($vo->idEtapa) && isset($vo->idEtapaToCopy) && isset($vo->idTemporada)) {
            
            $rsCnt = Db::executeQuery($this->GET_COUNT_LARGADAS, array($vo->idEtapaToCopy, $vo->idTemporada));

            if (count($rsCnt) > 0) {
                if($rsCnt[0]['cnt']) {
                    Db::executeSave($this->DELETE_LARGADA, array($vo->idEtapa, $vo->idTemporada));
                    $result = Db::executeSave($this->COPY_LARGADAS_FROM_ETAPA, array($vo->idEtapa, $vo->idEtapaToCopy, $vo->idTemporada));
                }
            }
        }

        return $result;
    }

    public function saveLargadas($vo) {
        
        if (isset($vo)) {

            $query = $this->DELETE_LARGADA;
            
            $params = array();
            array_push($params, $vo->idEtapa);
            array_push($params, $vo->idTemporada);
            
            if (isset($vo->largadas) && (count($vo->largadas) > 0)) {
                Db::executeSave($query, $params);

                $query = $this->SAVE_LARGADA;
                $results = array();
                foreach ($vo->largadas as $categoria) {
                    $params = array($vo->idEtapa, $vo->idTemporada, $categoria->idCategoria, $categoria->largada->horarioLargada, $categoria->largada->tempoProva);
                    $result = Db::executeSave($query, $params);
                    array_push($results, $result);
                }
            }    
        }

        return $results;
    }
    
    /**
        salvar uma etapa
    */
    public function saveEtapa($vo) {
        
        $params = array();
        
        array_push($params, $vo->data);
        array_push($params, $vo->local);
        array_push($params, $vo->descricao);
        
        if (isset($vo->idEtapa)) {
            array_push($params, $vo->publicarResultado);            
            array_push($params, $vo->idEtapa);
            
            return Db::executeSave($this->UPDATE_ETAPA, $params);
        } else {
            
            if (isset($vo->idTemporada)) {
                array_push($params, $vo->idTemporada);
            } else if (isset($vo->temporada) && isset($vo->temporada->idTemporada)) {
                array_push($params, $vo->temporada->idTemporada);
            }
            
            return Db::executeSave($this->INSERT_ETAPA, $params);
        }
        
        
        
        return false;
        
    }
    
}