<?php

require_once('../categoria/CategoriaVO.class.php');

class LargadaVO {

	public $idEtapa;
	public $idCategoria;
	public $idTemporada;
	public $horarioLargada;
	public $tempoProva;
    public $categoria;

	public function loadFromResultSet($rs) {
        $this->loadFromResultSetSuffix($rs, "");        
    }
    
    public function loadFromResultSetSuffix($rs, $suffix) {
        
        if (isset($rs['id_categoria'.$suffix])) {
            $this->idCategoria = $rs['id_categoria'.$suffix];
        } else if (isset($rs['id_categoria'])) { 
            $this->idCategoria = $rs['id_categoria'];
        }

        if (isset($rs['id_etapa'.$suffix])) {
            $this->idEtapa = $rs['id_etapa'.$suffix];
        } else if (isset($rs['id_etapa'])) { 
            $this->idEtapa = $rs['id_etapa']; 
        }

        if (isset($rs['id_temporada'.$suffix])) {
            $this->idTemporada = $rs['id_temporada'.$suffix];
        } else if (isset($rs['id_temporada'])) { 
            $this->idTemporada = $rs['id_temporada']; 
        }

        if (isset($rs['horario_largada'.$suffix])) { $this->horarioLargada = $rs['horario_largada'.$suffix]; }
        if (isset($rs['tempo_prova'.$suffix])) { $this->tempoProva = $rs['tempo_prova'.$suffix]; }
    }
}