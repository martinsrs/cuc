<?php

require_once('CategoriaDao.class.php');
require_once('CategoriaVO.class.php');

$dao = new CategoriaDao();
$vo = new CategoriaVO();

if (isset($_GET['id_temporada'])) {
    $vo->idTemporada = $_GET['id_temporada'];

	if (isset($_GET['id_categoria'])) {
		$vo->idCategoria = $_GET['id_categoria'];
	}

    if (isset($_GET['id_etapa'])) {
    	$vo->idEtapa = $_GET['id_etapa'];
		$result = $dao->listAllCategoriaWithResultsByEtapa($vo);
    } else {
    	$result = $dao->listAllCategoriaWithResults($vo);
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>