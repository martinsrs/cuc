<?php

require_once('CategoriaDao.class.php');
require_once('CategoriaVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $categoria = $request;
    
    $dao = new CategoriaDao();
    
    if (isset($categoria->idCategoria)) {
        if (isset($categoria->direction) && $categoria->direction == "up") {
            $result = $dao->moveUpCategory($categoria);
            
        } else if (isset($categoria->direction) && $categoria->direction == "down") {
            $result = $dao->moveDownCategory($categoria);
        }
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>