<?php

require_once('CategoriaDao.class.php');
require_once('CategoriaVO.class.php');

$dao = new CategoriaDao();
$vo = new CategoriaVO();

if (isset($_GET['id_categoria'])) {
    $vo->idCategoria = $_GET['id_categoria'];
    $result = $dao->getCategoriaById($vo);
} else if (isset($_GET['byTemporada'])) {
    $vo->idTemporada = $_GET['byTemporada']; 
    
    if (isset($_GET['onlySigned'])) {
        $vo->idEtapa = $_GET['id_etapa'];

        if (isset($_GET['combo'])) {
            $result = $dao->listCategoriaByTemporadaSignedOrderName($vo);
        } else {
            $result = $dao->listCategoriaByTemporadaSigned($vo);
        }

    } else {
        if (isset($_GET['combo'])) {
            $result = $dao->listCategoriaByTemporadaOrderName($vo);
        } else {
            $result = $dao->listCategoriaByTemporada($vo);
        }
    }
    
} else {
    $result = $dao->listAllCategoria();    
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>