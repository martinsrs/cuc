<?php

require_once('CategoriaDao.class.php');
require_once('CategoriaVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $novaCategoria = $request;
    
    $dao = new CategoriaDao();
    $result = $dao->saveCategoria($novaCategoria);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>