<?php

require_once('CategoriaDao.class.php');
require_once('CategoriaVO.class.php');

$dao = new CategoriaDao();
$vo = new CategoriaVO();

if (isset($_GET['id_temporada'])) {
    $vo->idTemporada = $_GET['id_temporada'];

    $result = $dao->listCategoriaByTemporadaChecked($vo);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>