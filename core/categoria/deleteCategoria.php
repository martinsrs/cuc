<?php

require_once('CategoriaDao.class.php');
require_once('CategoriaVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $categoria = $request;
    
    $dao = new CategoriaDao();
    
    if (isset($categoria->idCategoria)) {
        $result = $dao->deleteCategoria($categoria);
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>