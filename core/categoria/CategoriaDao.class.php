<?php
require_once('../db.inc.php');
require_once('CategoriaVO.class.php');

class CategoriaDao {
    
    private $SELECT_CATEGORIES  = "select c.id_categoria, ct.id_temporada, c.nome, c.criterio, c.valor_inscricao, c.ordem from categoria c join categoria_temporada ct on (ct.id_categoria = c.id_categoria) where ct.id_temporada = ?";

    private $SELECT_CATEGORY_BY_ID  = "select c.id_categoria, c.nome, c.criterio, c.valor_inscricao, c.ordem from categoria c where c.id_categoria = ?";
    private $SELECT_ALL_CATEGORIES = "select id_categoria, nome, criterio, valor_inscricao, ordem from categoria order by ordem asc";
    private $SELECT_ALL_CATEGORIES_ORDER_NAME = "select id_categoria, nome, criterio, valor_inscricao, ordem from categoria order by nome asc";
    
    private $SELECT_SIGNED_CATEGORIES_ORDER_NAME = "select c.id_categoria, ct.id_temporada, c.nome, c.criterio, c.valor_inscricao, c.ordem from categoria c 
                                        join categoria_temporada ct on (ct.id_categoria = c.id_categoria)
                                        join atleta_etapa ae on (ae.id_categoria = ct.id_categoria and ae.id_temporada = ct.id_temporada)
                                        where ct.id_temporada = ? and ae.id_etapa = ? group by c.id_categoria, ct.id_temporada, c.nome, c.criterio, c.ordem order by c.nome asc";  

    private $SELECT_SIGNED_CATEGORIES = "select c.id_categoria, ct.id_temporada, c.nome, c.criterio, c.valor_inscricao, c.ordem from categoria c 
                                        join categoria_temporada ct on (ct.id_categoria = c.id_categoria)
                                        join atleta_etapa ae on (ae.id_categoria = ct.id_categoria and ae.id_temporada = ct.id_temporada)
                                        where ct.id_temporada = ? and ae.id_etapa = ? group by c.id_categoria, ct.id_temporada, c.nome, c.criterio, c.ordem order by c.ordem asc";    
    
    private $DELETE_CATEGORY = "delete from categoria where id_categoria = ? ";    
    private $INSERT_CATEGORY = "insert into categoria (nome, criterio, valor_inscricao, ordem) values (?,?,?,?)";  
    private $UPDATE_CATEGORY = "update categoria set nome=?, criterio=?, valor_inscricao=?, ordem=? where id_categoria = ?";
    private $SELECT_MAX_ORDER = "select max(ordem)+1 from categoria";
    
    private $MOVE_UP_CATEGORY = "call move_up_category(?)";
    private $MOVE_DOWN_CATEGORY = "call move_down_category(?)";
    private $AJUST_ORDER = "call ajust_order()";    
    
    private $SELECT_CATEGORIES_BY_SEASON = "select c.id_categoria, c.nome, c.criterio, c.ordem from categoria c join categoria_temporada ct on (ct.id_categoria = c.id_categoria) where ct.id_temporada = ? order by ordem asc";

    private $SELECT_CATEGORIES_WITH_RESULTS = "select c.id_categoria, c.nome, c.criterio, c.valor_inscricao, c.ordem
                                                from categoria c
                                                join categoria_temporada ct on (ct.id_categoria = c.id_categoria)
                                                join atleta_etapa ae on (ae.id_categoria = c.id_categoria and ae.id_temporada = ct.id_temporada)
                                                join etapa e on (e.id_temporada = ae.id_temporada and e.id_etapa = ae.id_etapa)
                                                where e.publicar_resultado = 1
                                                and ct.id_temporada = ?
                                                #CATEGORIA#
                                                and ae.classificacao is not null
                                                group by c.id_categoria, c.nome, c.criterio, c.valor_inscricao, c.ordem
                                                order by c.nome, c.ordem";

    private $SELECT_CATEGORIES_WITH_RESULTS_BY_ETAPA = "select c.id_categoria, c.nome, c.criterio, c.valor_inscricao, c.ordem
                                                from categoria c
                                                join categoria_temporada ct on (ct.id_categoria = c.id_categoria)
                                                join atleta_etapa ae on (ae.id_categoria = c.id_categoria and ae.id_temporada = ct.id_temporada)
                                                join etapa e on (e.id_temporada = ae.id_temporada and e.id_etapa = ae.id_etapa)
                                                where e.publicar_resultado = 1
                                                and ct.id_temporada = ?
                                                and ae.id_etapa = ?
                                                #CATEGORIA#
                                                and ae.classificacao is not null
                                                group by c.id_categoria, c.nome, c.criterio, c.valor_inscricao, c.ordem
                                                order by c.nome, c.ordem";
    
    private function getListFromRS($rs) {
        $list = array();        
        foreach ($rs as $val) {
            $vo = new CategoriaVo();
            $vo->loadFromResultSet($val);
            array_push($list, $vo);            
        }        
        return $list;
    }
    
    /**
        Lista todas categorias por ordem
    */
    public function listAllCategoria() {
        $result = Db::executeQuery($this->SELECT_ALL_CATEGORIES, null);
        return $this->getListFromRS($result);
    }

    /**
        Lista categorias com resultados
    */
    public function listAllCategoriaWithResults($vo) {

        $query = str_replace("#CATEGORIA#", "", $this->SELECT_CATEGORIES_WITH_RESULTS);
        $params = array($vo->idTemporada);
        if ($vo->idCategoria != null) {
            array_push($params, $vo->idCategoria);
            $query = str_replace("#CATEGORIA#", "and c.id_categoria = ?", $this->SELECT_CATEGORIES_WITH_RESULTS);
        }

        $result = Db::executeQuery($query, $params);
        return $this->getListFromRS($result);
    }

    /**
        Lista categorias com resultados
    */
    public function listAllCategoriaWithResultsByEtapa($vo) {
        
        $query = str_replace("#CATEGORIA#", "", $this->SELECT_CATEGORIES_WITH_RESULTS_BY_ETAPA);
        $params = array($vo->idTemporada, $vo->idEtapa);
        if ($vo->idCategoria != null) {
            array_push($params, $vo->idCategoria);
            $query = str_replace("#CATEGORIA#", "and c.id_categoria = ?", $this->SELECT_CATEGORIES_WITH_RESULTS_BY_ETAPA);
        }

        $result = Db::executeQuery($query, $params);
        return $this->getListFromRS($result);
    }

    /**
        Lista todas categorias por nome
    */
    public function listAllCategoriaOrderByName() {
        $result = Db::executeQuery($this->SELECT_ALL_CATEGORIES_ORDER_NAME, null);
        return $this->getListFromRS($result);
    }
    
    public function getCategoriaById($vo) {
        $this->SELECT_CATEGORY_BY_ID;
        $retorno = array();
        
        if (isset($vo) && isset($vo->idCategoria)) {
            $params = array($vo->idCategoria);
            return $this->getListFromRS(Db::executeQuery($this->SELECT_CATEGORY_BY_ID, $params));
        }            
        
        return $retorno;            
    }
    

    /**
        busca as categorias por temporada, para popular combos
    */
    public function listCategoriaByTemporadaOrderName($vo) {
        
        $params = array($vo->idTemporada);
        $all = Db::executeQuery($this->SELECT_ALL_CATEGORIES_ORDER_NAME, null);        
        $selected = Db::executeQuery($this->SELECT_CATEGORIES_BY_SEASON, $params);
        
        $retorno = array();
        foreach ($all as $categoria) {
            $checked = false;
            foreach ($selected as $marcadas) {
                if ($categoria['id_categoria'] == $marcadas['id_categoria']) {
                    $checked = true;
                }
            }
            $categoria['checked'] = $checked;
            array_push($retorno, $categoria);
        }
        
        return $retorno;
    }

    /**
        busca as categorias por temporada, para popular checkboxes
    */
    public function listCategoriaByTemporada($vo) {
        
        $params = array($vo->idTemporada);
        $all = Db::executeQuery($this->SELECT_ALL_CATEGORIES, null);        
        $selected = Db::executeQuery($this->SELECT_CATEGORIES_BY_SEASON, $params);
        
        $retorno = array();
        foreach ($all as $categoria) {
            $checked = false;
            foreach ($selected as $marcadas) {
                if ($categoria['id_categoria'] == $marcadas['id_categoria']) {
                    $checked = true;
                }
            }
            $categoria['checked'] = $checked;
            array_push($retorno, $categoria);
        }
        
        return $retorno;
    }
    
    public function listCategoriaByTemporadaSignedOrderName($vo) {
        $params = array($vo->idTemporada, $vo->idEtapa);      
        $selected = Db::executeQuery($this->SELECT_SIGNED_CATEGORIES_ORDER_NAME, $params);
        
        $retorno = $selected;
        
        return $retorno;
    }
    
    public function listCategoriaByTemporadaSigned($vo) {
        $params = array($vo->idTemporada, $vo->idEtapa);      
        $selected = Db::executeQuery($this->SELECT_SIGNED_CATEGORIES, $params);
        
        $retorno = $selected;
        
        return $retorno;
    }
    
    public function listCategoriaByTemporadaChecked($categoriaVo) {
        $params = array($categoriaVo->idTemporada);
        $result = Db::executeQuery($this->SELECT_CATEGORIES_BY_SEASON, $params);

        return $this->getListFromRS($result);
    }
    
    public function listCategoria($categoriaVo) {
        $params = array($categoriaVo->idTemporada);
        $result = Db::executeQuery($this->SELECT_CATEGORIES, $params);

        return $this->getListFromRS($result);
    }
    
    /**
        Insere ou salva uma nova categoria
    */
    public function saveCategoria($categoriaVo) {
        
        $query = $this->INSERT_CATEGORY;

        if (isset($categoriaVo)) {
            
            if(!isset($categoriaVo->ordem)) {
                $order = Db::executeQuery($this->SELECT_MAX_ORDER, null);
                $categoriaVo->ordem = $order[0][0];
            }

            $params = array($categoriaVo->nome, $categoriaVo->criterio, $categoriaVo->valorInscricao, $categoriaVo->ordem);
            
            if (isset($categoriaVo->idCategoria) && $categoriaVo->idCategoria != null) {
                $query = $this->UPDATE_CATEGORY;
                array_push($params, $categoriaVo->idCategoria);
            }
            
            return Db::executeSave($query,$params);
        }
        
        return false;
    }
    
    /**
        Exclui a categoria
    */
    public function deleteCategoria($categoriaVo) {
        $query = $this->DELETE_CATEGORY;
        
        if (isset($categoriaVo)) {
            
            if (isset($categoriaVo->idCategoria) && $categoriaVo->idCategoria != null) {
                $params = array($categoriaVo->idCategoria);
                $result = Db::executeSave($query,$params);
                Db::executeSave($this->AJUST_ORDER, array());
                                
                return $result;
            }
        }
        
        return false;
    }
    
    public function moveUpCategory($categoriaVo) {
        
        if (isset($categoriaVo) && isset($categoriaVo->idCategoria)) {
            $params = array($categoriaVo->idCategoria);
            return Db::executeSave($this->MOVE_UP_CATEGORY,$params);                
        }
        
        return false;        
    }
    
    public function moveDownCategory($categoriaVo) {
        
        if (isset($categoriaVo) && isset($categoriaVo->idCategoria)) {
            $params = array($categoriaVo->idCategoria);
            return Db::executeSave($this->MOVE_DOWN_CATEGORY,$params);                
        }
        
        return false;        
    }    
}    

?>