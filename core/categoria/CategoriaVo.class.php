<?php

class CategoriaVO {
    
    public $idCategoria;
    public $nome;
    public $criterio;
    public $ordem;
    public $valorInscricao;
    private $temporadaVo;
    
    public function loadFromResultSet($rs) {
        $this->loadFromResultSetSuffix($rs, "");        
    }
    
    public function loadFromResultSetSuffix($rs, $suffix) {
        
        if (isset($rs['id_categoria'.$suffix])) {
            $this->idCategoria = $rs['id_categoria'.$suffix];
        } else if (isset($rs['id_categoria'])) { 
            $this->idCategoria = $rs['id_categoria']; 
        }
        
        if (isset($rs['nome'.$suffix])) { $this->nome = $rs['nome'.$suffix]; }
        if (isset($rs['criterio'.$suffix])) { $this->criterio = $rs['criterio'.$suffix]; }
        if (isset($rs['valor_inscricao'.$suffix])) { $this->valorInscricao = $rs['valor_inscricao'.$suffix]; }
        if (isset($rs['ordem'.$suffix])) { $this->ordem = $rs['ordem'.$suffix]; }
    }
}

?>