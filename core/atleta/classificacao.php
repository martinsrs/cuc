<?php
require_once('AtletaVO.class.php');
require_once('AtletaDao.class.php');
require_once('../filter/ClassificacaoFilter.class.php');

$dao = new AtletaDao();
$vo = new AtletaVO();

$result = false;
if(isset($_GET['id_temporada']) && isset($_GET['id_etapa']) && isset($_GET['id_categoria'])) {
    $filter = new ClassificacaoFilter($_GET['id_temporada'],$_GET['id_etapa'],$_GET['id_categoria']);
    $result = $dao->listClassificacao($filter);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>