<?php
require_once('AtletaVO.class.php');
require_once('AtletaDao.class.php');

$dao = new AtletaDao();
$vo = new AtletaVO();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

if ($request) {
    $vo = $request;
    $result = $dao->deleteAtleta($vo);
} else {
    $result = false;
} 

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>