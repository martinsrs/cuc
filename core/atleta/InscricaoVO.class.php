<?php
require_once('../db.inc.php');
require_once('AtletaVO.class.php');
require_once('../equipe/EquipeVO.class.php');
require_once('../etapa/EtapaVO.class.php');
require_once('../categoria/CategoriaVO.class.php');

class InscricaoVO {
    
    public $atleta;
    public $equipe;
    public $etapa;
    public $categoria;
    public $numero;
    
    public function loadFromResultSet($rs) {
        
        if (isset($rs['id_atleta'])) { 
            $this->atleta = new AtletaVO();
            $this->atleta->loadFromResultSetSuffix($rs, "_atl");
        }
        
        if (isset($rs['id_equipe_insc'])) {
            $this->equipe = new EquipeVO();
            $this->equipe->loadFromResultSetSuffix($rs, "_equipe_insc");
        }
        
        if (isset($rs['id_etapa'])) {
            $this->etapa = new EtapaVO();
            $this->etapa->loadFromResultSet($rs);
        }
        
        if (isset($rs['id_categoria'])) {
            $this->categoria = new CategoriaVO();
            $this->categoria->loadFromResultSetSuffix($rs, "_categoria");
        }
        
        if (isset($rs['numero'])) { $this->numero = $rs['numero']; }
        //if (isset($rs['classificacao'])) { $this->classificacao = $rs['classificacao']; }
        
    }
}
?>