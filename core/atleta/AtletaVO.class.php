<?php
require_once('../db.inc.php');
require_once('../equipe/EquipeVO.class.php');

class AtletaVO {
    
    public $idAtleta;
    public $nome;
    public $cidade;
    public $estado;
    
    public $equipe;
    
    public function loadFromResultSet($rs) {
        $this->loadFromResultSetSuffix($rs, ""); 
    }
    
    public function loadFromResultSetSuffix($rs, $suffix) {
        
        if (isset($rs['id_atleta'])) { $this->idAtleta = $rs['id_atleta']; }
        if (isset($rs['nome'.$suffix])) { $this->nome = $rs['nome'.$suffix]; }
        if (isset($rs['cidade'.$suffix])) { $this->cidade = $rs['cidade'.$suffix]; }
        if (isset($rs['estado'.$suffix])) { $this->estado = $rs['estado'.$suffix]; }
        
        $suffix .= "_equipe";
        
        if (isset($rs['id_equipe']) || isset($rs['id_equipe'.$suffix])) {
            $this->equipe = new EquipeVO();
            $this->equipe->loadFromResultSetSuffix($rs, $suffix);
        }
        
    }
}
?>