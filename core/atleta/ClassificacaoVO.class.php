<?php
require_once('InscricaoVO.class.php');


class ClassificacaoVO extends InscricaoVO {
    
    public $classificacao;
    public $pontos;
    
    public function loadFromResultSet($rs) {
        parent::loadFromResultSet($rs);
        
        if (isset($rs['classificacao'])) { $this->classificacao = $rs['classificacao']; }
        if (isset($rs['pontos'])) { $this->pontos = $rs['pontos']; }
        
    }
    
}

?>
