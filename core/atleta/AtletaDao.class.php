<?php
require_once('../db.inc.php');
require_once('AtletaVO.class.php');
require_once('InscricaoVO.class.php');
require_once('ClassificacaoVO.class.php');

class AtletaDao {
    
    private $SELECT_ALL_ATLETAS = "select a.id_atleta, a.nome, a.cidade, a.estado, a.id_equipe, e.nome as nome_equipe, e.cidade as cidade_equipe, e.estado as estado_equipe from atleta a join equipe e on (e.id_equipe = a.id_equipe) order by a.nome, e.nome";
    private $SELECT_ATLETA_BY_ID = "select id_atleta, nome, cidade, estado, id_equipe from atleta a where id_atleta = ?";
    private $SELECT_ATLETA_BY_QUERY = "select a.id_atleta, a.nome, a.cidade, a.estado, a.id_equipe, e.nome as nome_equipe, e.cidade as cidade_equipe, e.estado as estado_equipe from atleta a join equipe e on (e.id_equipe = a.id_equipe) where upper(a.nome) like upper(?) or upper(e.nome) like upper(?) order by a.nome, e.nome";
    private $INSERT_ATLETA = "insert into atleta (nome, cidade, estado, id_equipe) values (?, ?, ?, ?)";
    private $UPDATE_ATLETA = "update atleta set nome=?, cidade=? , estado=?, id_equipe=? where id_atleta=?";
    private $DELETE_ATLETA = "delete from atleta where id_atleta=?";
    
    private $INSCRICAO = "insert into atleta_etapa (id_atleta, id_etapa, id_temporada,id_equipe,numero,id_categoria) values (?,?,?,?,?,?)";
    private $REMOVE_INSCRICAO = "delete from atleta_etapa where id_atleta = ? and id_etapa = ?";
    
    private $GET_INSCRICOES = "select a.id_atleta, a.nome as nome_atl, a.cidade as cidade_atl, a.estado as estado_atl, a.id_equipe, 
                                       ea.nome as nome_atl_equipe, ea.cidade as cidade_atl_equipe, ea.estado as estado_atl_equipe,
                                       ae.id_atleta, ae.id_etapa, ae.id_temporada, ae.classificacao,
                                       ae.id_equipe as id_equipe_insc, ae.numero, ae.id_categoria,
                                       ea.nome as nome_equipe_insc, ea.cidade as cidade_equipe_insc, ea.estado as estado_equipe_insc,
                                       e.data, e.descricao, e.local, e.publicar_resultado,
                                       t.ano, t.edicao, t.publicado,
                                       c.nome as nome_categoria, c.criterio as criterio_categoria, c.ordem as ordem_categoria
                                from atleta a
                                join equipe ea on (ea.id_equipe = a.id_equipe)
                                join atleta_etapa ae on (ae.id_atleta = a.id_atleta)
                                join equipe aee on (aee.id_equipe = a.id_equipe)
                                join etapa e on (e.id_etapa = ae.id_etapa)
                                join temporada t on (t.id_temporada = ae.id_temporada)
                                join categoria c on (c.id_categoria = ae.id_categoria)";

    private $GET_CLASSIFICACAO = "select a.id_atleta, a.nome as nome_atl, a.cidade as cidade_atl, a.estado as estado_atl, a.id_equipe, 
                                           ea.nome as nome_atl_equipe, ea.cidade as cidade_atl_equipe, ea.estado as estado_atl_equipe,
                                           ae.id_atleta, ae.id_etapa, ae.id_temporada,
                                           CASE
                                                WHEN ae.classificacao = '999' THEN 'NC'
                                                ELSE ae.classificacao
                                            END as classificacao,  
                                           ae.id_equipe as id_equipe_insc, ae.numero, ae.id_categoria,
                                           ea.nome as nome_equipe_insc, ea.cidade as cidade_equipe_insc, ea.estado as estado_equipe_insc,
                                           e.data, e.local, e.publicar_resultado,
                                           t.ano, t.edicao, t.publicado,
                                           c.nome as nome_categoria, c.criterio as criterio_categoria, c.ordem as ordem_categoria
                                    from atleta a
                                    join equipe ea on (ea.id_equipe = a.id_equipe)
                                    join atleta_etapa ae on (ae.id_atleta = a.id_atleta)
                                    join equipe aee on (aee.id_equipe = a.id_equipe)
                                    join etapa e on (e.id_etapa = ae.id_etapa)
                                    join temporada t on (t.id_temporada = ae.id_temporada)
                                    join categoria c on (c.id_categoria = ae.id_categoria)
                                    
                                    where t.id_temporada = ?
                                    and e.id_etapa = ?
                                    and c.id_categoria = ?
                                    
                                    order by ae.classificacao";

    private $UPDATE_CLASSIFICACAO = "update atleta_etapa set classificacao=? where id_temporada = ? and id_etapa = ? and id_categoria=? and id_atleta = ?";

    /**
     * @param $rs
     * @return array
     */
    private function getListFromRS($rs) {
        $list = array();
        foreach ($rs as $val) {
            $atleta = new AtletaVO();
            $atleta->loadFromResultSet($val);
            array_push($list, $atleta);            
        }        
        return $list;
    }

    /**
     * @param $rs
     * @return array
     */
    private function getListInscricoesFromRS($rs) {
        $list = array();
        foreach ($rs as $val) {
            $vo = new InscricaoVO();
            $vo->loadFromResultSet($val);
            array_push($list, $vo);            
        }        
        return $list;
    }
    
    /**
     * @param $rs
     * @return array
     */
    private function getListClassificacaoFromRS($rs) {
        $list = array();
        foreach ($rs as $val) {
            $vo = new ClassificacaoVO();
            $vo->loadFromResultSet($val);
            array_push($list, $vo);            
        }        
        return $list;
    }

    /**
     * @param $vo
     * @return bool
     */
    public function saveAtleta($vo) {
        $query = $this->INSERT_ATLETA;
        $idEquipe = 0;
        
        if (isset($vo->equipe) && isset($vo->equipe->idEquipe)) {
            $idEquipe = $vo->equipe->idEquipe;
        }
        $params = array($vo->nome, $vo->cidade, $vo->estado, $idEquipe);
        
        if (isset($vo->idAtleta)) {
            $query = $this->UPDATE_ATLETA;
            array_push($params, $vo->idAtleta);
        }
        return Db::executeSave($query, $params);
    }

    /**
     * Deleta Atleta
     * @param $vo
     * @return bool
     */
    public function deleteAtleta($vo) {
    
        if (isset($vo->idAtleta)) {
            $params = array($vo->idAtleta);
            
            return Db::executeSave($this->DELETE_ATLETA, $params);
        }
       
        return false;
    }
    
    /**
     * Lista todos Atletas
     * @return array
    */
    public function listAtleta() {
        $result = Db::executeQuery($this->SELECT_ALL_ATLETAS, null);
        return $this->getListFromRS($result);
    }
    
    
    /**
     * Lista todos Atletas
     * @param query
     * @return array
    */
    public function listAtletaByQuery($query) {
        $queryStr = "%".$query."%";
        
        $result = Db::executeQuery($this->SELECT_ATLETA_BY_QUERY, array($queryStr,$queryStr));
        return $this->getListFromRS($result);
    }

    /**
     * Lista as Inscrições
     * @param $idAtleta
     * @param $idEtapa
     * @return array
     */
    public function listInscricoes($idAtleta, $idEtapa) {
    
        $query = $this->GET_INSCRICOES;
        $params = array();
        if ($idAtleta > 0) {
            $params = array($idAtleta);
            $query .= " where a.id_atleta = ? ";
        }
        
        if ($idEtapa > 0) {
            array_push($params, $idEtapa);
            $query .= " and e.id_etapa = ? ";
        }
        
        $query .= " order by a.nome, c.ordem";
            
        $result = Db::executeQuery($query, $params);
        return $this->getListInscricoesFromRS($result);
    }

    /**
     * Salva classificacao
     * @param $list
     * @return array
     */
    public function saveClassificacao($list) {

        $results = array();
        if ($list) {
            foreach ($list as $vo) {
                $params = array();

                if (is_numeric($vo->classificacao)) {
                    array_push($params, $vo->classificacao);
                } else {
                    array_push($params, 999);
                }
                array_push($params, $vo->etapa->temporada->idTemporada);
                array_push($params, $vo->etapa->idEtapa);
                array_push($params, $vo->categoria->idCategoria);
                array_push($params, $vo->atleta->idAtleta);

                $result = Db::executeSave($this->UPDATE_CLASSIFICACAO, $params);
                array_push($results, $result);
            }
        }

        return $results;
    }

    /**
     * Listar Classificação
     * @param ClassificacaoFilter
     * @return array
     */
    public function listClassificacao($classificacaoFilter) {
        $query = $this->GET_CLASSIFICACAO;

        $params = array();
        array_push($params, $classificacaoFilter->idTemporada);
        array_push($params, $classificacaoFilter->idEtapa);
        array_push($params, $classificacaoFilter->idCategoria);

        $result = Db::executeQuery($query, $params);
        return $this->getListClassificacaoFromRS($result);
    }

    /**
     * Realiza a inscrição do Atleta
     * @param vo
     * @return array
     */
    public function inscreverAtleta($vo) {
        
        $results = array();
        
        Db::executeSave($this->REMOVE_INSCRICAO, array($vo->atleta->idAtleta, $vo->idEtapa));
        
        if ($vo->categoriasSelecionadas != null) {
            foreach ($vo->categoriasSelecionadas as $categoria) {
                $params = array();
                array_push($params, $vo->atleta->idAtleta);
                array_push($params, $vo->idEtapa);
                array_push($params, $vo->idTemporada);
                array_push($params, $vo->atleta->equipe->idEquipe);
                array_push($params, $categoria->numero);
                array_push($params, $categoria->idCategoria);
                
                $result = Db::executeSave($this->INSCRICAO, $params);            
                array_push($results, $result);
            }
        }
        
        return $results;        
    }
    
}