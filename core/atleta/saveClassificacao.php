<?php

require_once('AtletaVO.class.php');
require_once('AtletaDao.class.php');
require_once('InscricaoVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $vo = $request;
    $dao = new AtletaDao();
    $result = $dao->saveClassificacao($vo);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);
