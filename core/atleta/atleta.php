<?php
require_once('AtletaVO.class.php');
require_once('AtletaDao.class.php');

$dao = new AtletaDao();
$vo = new AtletaVO();


if(isset($_GET['inscricao'])) {
    $inscricao = $_GET['inscricao'];
    $idAtleta = 0;
    $idEtapa = 0;

    if(isset($_GET['id_atleta'])) {
        $idAtleta = $_GET['id_atleta'];
    }
    if(isset($_GET['id_etapa'])) {
        $idEtapa = $_GET['id_etapa'];
    }
    $result = $dao->listInscricoes($idAtleta, $idEtapa);
    
} else if (isset($_GET['id_atleta'])) {
    $vo->idAtleta = $_GET['id_atleta'];
    
} else if(isset($_GET['query'])) {
    $query = $_GET['query'];
    $result = $dao->listAtletaByQuery($query);
    
} else {
    $result = $dao->listAtleta();
} 

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>