<?php

require_once('TemporadaDao.class.php');
require_once('TemporadaVO.class.php');
require_once('../filter/QueryFilter.class.php');

$dao = new TemporadaDao();
$vo = new TemporadaVO();

if (isset($_GET['id_temporada'])) {
    $vo->idTemporada = $_GET['id_temporada'];
    $result = $dao->getTemporada($vo);
} else {
    
    if (isset($_GET['all'])) {
        $result = $dao->listAllTemporada();
    } else if (isset($_GET['etapa'])) {

    	$filter = new QueryFilter(null,null);
    	if (isset($_GET['pageSize']) && isset($_GET['currentPage'])) {
    		$filter->pageSize = $_GET['pageSize'];
    		$filter->currentPage = $_GET['currentPage'];
    	}

    	$result = $dao->listTemporadaEtapa($filter);
    } else {
        $result = $dao->listTemporada();
    }   
    
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>