<?php

require_once('TemporadaDao.class.php');
require_once('TemporadaVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $vo = $request;
    
    $dao = new TemporadaDao();
    
    if (isset($vo->idTemporada)) {
        $result = $dao->assignCategorias($vo);
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>