<?php

require_once('../atleta/AtletaVO.class.php');
require_once('../equipe/EquipeVO.class.php');

class RankingVO {

	public $atleta;
	public $posicao;
	public $totalPontos;

	public function loadFromResultSet($rs) {
        
        if (isset($rs['id_atleta'])) { 
            $this->atleta = new AtletaVO();
            $this->atleta->loadFromResultSetSuffix($rs, "_atl");
        }
        
        if (isset($rs['total_pontos'])) { $this->totalPontos = $rs['total_pontos']; }
        if (isset($rs['classificacao'])) { $this->posicao = $rs['classificacao']; }        
    }
}

?>