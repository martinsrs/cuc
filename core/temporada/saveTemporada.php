<?php

require_once('TemporadaDao.class.php');
require_once('TemporadaVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $novaTemporada = $request;
    
    if (isset($novaTemporada->copyCategory) && $novaTemporada->copyCategory) {
        $copyFrom = $novaTemporada->copyCategory;
    }
    
    $dao = new TemporadaDao();

    $result = $dao->saveTemporada($novaTemporada);

    if ($result && isset($copyFrom)) {
        $voCopia = new TemporadaVO();
        $voCopia->idTemporada = $copyFrom;
        $novaTemporada->idTemporada = $result;
        
        $resultCopy = $dao->copyCategories($voCopia, $novaTemporada);
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>