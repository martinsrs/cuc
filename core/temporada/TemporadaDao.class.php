<?php
require_once('../db.inc.php');
require_once('../filter/ClassificacaoFilter.class.php');
require_once('RankingVO.class.php');
require_once('RankingTeamVO.class.php');
require_once('../etapa/EtapaVO.class.php');
require_once('../filter/QueryFilter.class.php');
require_once('../dvo/PaginatedList.class.php');

class TemporadaDao {
    
    private $INSERT_SEASON = "insert into temporada (ano, edicao, publicado) values (?, ?, ?) ";
    private $UPDATE_SEASON = "update temporada set ano=?, edicao=?, publicado=? where id_temporada = ? ";
    private $DELETE_SEASON = "delete from temporada where id_temporada = ? ";
    private $SELECT_ALL_SEASONS = "select id_temporada, ano, edicao, publicado from temporada order by ano desc";
    private $SELECT_AVAILABE_SEASONS = "select id_temporada, ano, edicao, publicado from temporada where publicado = 1 order by ano desc";
    private $SELECT_SEASON_BY_ID = "select id_temporada, ano, edicao, publicado from temporada where id_temporada = ?";
 
    private $COPY_CATEGORIES_FROM_SEASON = "insert into categoria_temporada (id_categoria, id_temporada) select id_categoria, ? from categoria_temporada ct where ct.id_temporada = ? and id_categoria not in (select id_categoria from categoria_temporada where id_temporada = ?)";
    
    private $ASSIGN_CATEGORIES = "insert into categoria_temporada (id_temporada, id_categoria) values (?,?)";
    private $REMOVE_CATEGORIES_FROM_SEASON = "delete from categoria_temporada where id_temporada = ?";

    private $GET_RANKING_BY_STEP = "select id_atleta, nome_atl, estado_atl, id_equipe, cidade_atl, 
                                            nome_equipe as nome_atl_equipe, cidade_equipe as cidade_atl_equipe, estado_equipe as estado_atl_equipe,
                                            classificacao,
                                            sum(pontos) as total_pontos
                                    from (
                                        select a.id_atleta, a.nome as nome_atl, a.cidade as cidade_atl, a.estado as estado_atl, a.id_equipe, 
                                               ea.nome as nome_equipe, ea.cidade as cidade_equipe, ea.estado as estado_equipe,
                                               c.nome,
                                               
                                               CASE
                                                    WHEN ae.classificacao = '999' THEN 'NC'
                                                    ELSE ae.classificacao
                                                END as classificacao,
                                                
                                                CASE
                                                    WHEN (p.pontos is null and ae.classificacao is not null) THEN (select min(pontos) from pontuacao where colocacao <> 999)
                                                    ELSE p.pontos
                                                END as pontos
                                                
                                        from atleta a
                                        join atleta_etapa ae on (ae.id_atleta = a.id_atleta)
                                        join equipe ea on (ea.id_equipe = ae.id_equipe)
                                        join temporada t on (t.id_temporada = ae.id_temporada)
                                        join categoria c on (c.id_categoria = ae.id_categoria)
                                        join etapa e on (e.id_etapa = ae.id_etapa)
                                        left outer join pontuacao p on (p.colocacao = ae.classificacao)
                                        where t.id_temporada = ?
                                        and c.id_categoria = ?
                                        and e.id_etapa = ?
                                        and e.publicar_resultado = 1
                                    ) as resultados
                                    group by id_atleta
                                    order by total_pontos desc, classificacao asc";


    private $GET_RANKING_SEASON = "select id_atleta, nome_atl, estado_atl, id_equipe, cidade_atl, 
                                            nome_equipe as nome_atl_equipe, cidade_equipe as cidade_atl_equipe, estado_equipe as estado_atl_equipe,
                                            sum(pontos) as total_pontos
                                    from (
                                        select a.id_atleta, a.nome as nome_atl, a.cidade as cidade_atl, a.estado as estado_atl, a.id_equipe, 
                                               ea.nome as nome_equipe, ea.cidade as cidade_equipe, ea.estado as estado_equipe,
                                               c.nome,
                                                
                                                CASE
                                                    WHEN (p.pontos is null and ae.classificacao is not null) THEN (select min(pontos) from pontuacao where colocacao <> 999)
                                                    ELSE p.pontos
                                                END as pontos
                                                
                                        from atleta a
                                        join equipe ea1 on (ea1.id_equipe = a.id_equipe)
                                        join atleta_etapa ae on (ae.id_atleta = a.id_atleta)
                                        join equipe ea on (ea.id_equipe = ae.id_equipe)
                                        join temporada t on (t.id_temporada = ae.id_temporada)
                                        join categoria c on (c.id_categoria = ae.id_categoria)
                                        join etapa e on (e.id_etapa = ae.id_etapa)
                                        left outer join pontuacao p on (p.colocacao = ae.classificacao)
                                        where t.id_temporada = ?
                                        and c.id_categoria = ?
                                        and e.publicar_resultado = 1
                                    ) as resultados
                                    group by  id_atleta
                                    order by total_pontos desc";
    
    private $GET_RANKING_SEASON_TEAM = "select id_equipe, nome, cidade, estado, sum(pontos) as total_pontos
                                        from (
                                            select e.id_equipe, e.nome, e.cidade, e.estado,
                                                    CASE
                                                        WHEN (p.pontos is null and ae.classificacao is not null) THEN (select min(pontos) from pontuacao where colocacao <> 999)
                                                        ELSE p.pontos
                                                    END as pontos

                                            from equipe e
                                            join atleta_etapa ae on (ae.id_equipe = e.id_equipe)
                                            join etapa et on (et.id_etapa = ae.id_etapa)
                                            left outer join pontuacao p on (p.colocacao = ae.classificacao)
                                            where et.publicar_resultado = 1
                                            and e.id_equipe > 0
                                            and ae.id_temporada = ?
                                        ) as tab
                                        group by id_equipe
                                        order by total_pontos desc";

    private $LIST_TEMPORADA_ETAPA = "select t.id_temporada, t.ano, t.edicao, t.publicado,
                                           e.id_etapa, e.data, e.local, e.descricao, e.publicar_resultado
                                    from temporada t
                                    join etapa e on (e.id_temporada = t.id_temporada)
                                    where t.publicado = 1
                                    order by t.ano desc, e.data desc";

    private $LIMIT = " limit ? offset ? ";


    /**
     * @param $rsort(array)
     * @return array
     */
    private function getListRankingRS($rs) {
        $list = array();
        foreach ($rs as $val) {
            $vo = new RankingVO();
            $vo->loadFromResultSet($val);
            array_push($list, $vo);            
        }        
        return $list;
    }

    /**
     * @param $rs
     * @return array
     */
    private function getListRankingTeamRS($rs) {
        $list = array();
        foreach ($rs as $val) {
            $vo = new RankingTeamVO();
            $vo->loadFromResultSet($val);
            array_push($list, $vo);            
        }        
        return $list;
    }

    private function getListTemporadaEtapaRS($rs, $totalRows) {

        $list = array();
        $currID = 0;
        $temporada;

        foreach ($rs as $val) {
            if (isset($val['id_temporada']) && $currID != $val['id_temporada']) { 
                $currID = $val['id_temporada'];

                $temporada = new TemporadaVO();
                $temporada->loadFromResultSet($val);
                $temporada->etapas = array();

                array_push($list, $temporada);
            }

            $etapa = new EtapaVO();
            $etapa->loadFromResultSetNoChild($val);
            array_push($temporada->etapas, $etapa);
        }

        $result = new PaginatedList();
        $result->totalRows = $totalRows;
        $result->list = $list;

        return $result;
    }

    public function listTemporadaEtapa($filter) {

        $query = $this->LIST_TEMPORADA_ETAPA;
        $params = null;

        if ($filter->checkFields()) {
            $query = $this->LIST_TEMPORADA_ETAPA . $this->LIMIT;

            $page = intval($filter->pageSize) * intval($filter->currentPage);
            $params = array(intval($filter->pageSize), intval($page));
        }

        $SQL_COUNT = " select count(*) as total_rows from (".$this->LIST_TEMPORADA_ETAPA.") as t";
        $r = Db::executeQuery($SQL_COUNT, null);
        $result = Db::executeQuery($query, $params);

        $paginatedList = $this->getListTemporadaEtapaRS($result, $r[0]['total_rows']);
        $paginatedList->pageSize = $filter->pageSize;
        $paginatedList->currentPage = $filter->currentPage;

        return $paginatedList;
    }

    /**
        Lista o Ranking dos atletas
    */
    public function listRanking($filter) {

        $query = $this->GET_RANKING_SEASON;

        $params = array();
        array_push($params, $filter->idTemporada);
        array_push($params, $filter->idCategoria);

        if (isset($filter->idEtapa) && $filter->idEtapa != null) {
            $query = $this->GET_RANKING_BY_STEP;
            array_push($params, $filter->idEtapa);
        } else {
            $query = str_replace('#CLASSIFICACAO#', '' , $query);
            $query = str_replace('#ID_ETAPA_FILTER#', '', $query);
        }

        $result = Db::executeQuery($query, $params);

        return $this->getListRankingRS($result);
    }

    /**
        Lista o Ranking das equipes
    */
    public function listRankingTeam($filter) {

        $query = $this->GET_RANKING_SEASON_TEAM;

        $params = array();
        array_push($params, $filter->idTemporada);
        $result = Db::executeQuery($query, $params);

        return $this->getListRankingTeamRS($result);
    }

    /**
        Lista temporadas
    */
    public function listTemporada() {
        return Db::executeQuery($this->SELECT_AVAILABE_SEASONS, null);
    }
    
    /**

        Lista todas temporadas
    */
    public function listAllTemporada() {
        return Db::executeQuery($this->SELECT_ALL_SEASONS, null);
    }
        
    /**
        Busca uma temporada especifica
    */
    public function getTemporada($temporadaVo) {
        $params = array($temporadaVo->idTemporada);
        return Db::executeQuery($this->SELECT_SEASON_BY_ID, $params);
    }
    
    /**
        Insere ou salva uma nova temporada
    */
    public function saveTemporada($temporadaVo) {
        
        $query = $this->INSERT_SEASON;
        
        if (isset($temporadaVo)) {
            
            if (!isset($temporadaVo->publicado)) {
                $temporadaVo->publicado = 0;
            }
            
            $params = array($temporadaVo->ano, $temporadaVo->edicao, $temporadaVo->publicado);
            
            if (isset($temporadaVo->idTemporada) && $temporadaVo->idTemporada != null) {
                $query = $this->UPDATE_SEASON;
                array_push($params, $temporadaVo->idTemporada);
            }
            
            return Db::executeSave($query,$params);
        }
        
        return false;
    }
    
    /**
        Inserir categorias para a temporada
    */
    public function assignCategorias($vo) {
        if (isset($vo)) {
            //limpa as temporadas
            Db::executeSave($this->REMOVE_CATEGORIES_FROM_SEASON, array($vo->idTemporada));
            
            foreach ($vo->categorias as $idCategoria) {
                //adiciona temporadas selecionadas
                Db::executeSave($this->ASSIGN_CATEGORIES, array($vo->idTemporada, $idCategoria));
            }
        } else {
            return false;
        }
        
        return true;
    }
    
    /**
        Exclui a temporada
    */
    public function deleteTemporada($temporadaVo) {
        $query = $this->DELETE_SEASON;
        
        if (isset($temporadaVo)) {
            
            if (isset($temporadaVo->idTemporada) && $temporadaVo->idTemporada != null) {
                $params = array($temporadaVo->idTemporada);
                
                return Db::executeSave($query,$params);
            }
        }
        
        return false;
    }
    
    /**
        Copia categorias de outra temporada
    */
    public function copyCategories($from, $to) {
        $query = $this->COPY_CATEGORIES_FROM_SEASON;
        
        $params = array();
        
        array_push($params, $to->idTemporada); 
        array_push($params, $from->idTemporada); 
        array_push($params, $to->idTemporada);
        
        return Db::executeSave($query,$params);        
    }
        
}    

?>