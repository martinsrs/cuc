<?php

require_once('TemporadaDao.class.php');
require_once('TemporadaVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $temporada = $request;
    
    $dao = new TemporadaDao();
    
    if (isset($temporada->idTemporada)) {
        $result = $dao->deleteTemporada($temporada);
    }
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>