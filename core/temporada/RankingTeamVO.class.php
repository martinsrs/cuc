<?php

require_once('../equipe/EquipeVO.class.php');

class RankingTeamVO {

	public $equipe;
	public $posicao;
	public $totalPontos;

	public function loadFromResultSet($rs) {
        
        if (isset($rs['id_equipe'])) { 
            $this->equipe = new EquipeVO();
            $this->equipe->loadFromResultSet($rs);
        }
        
        if (isset($rs['total_pontos'])) { $this->totalPontos = $rs['total_pontos']; }       
    }
}

?>