<?php

require_once('TemporadaDao.class.php');
require_once('TemporadaVO.class.php');
require_once('../filter/ClassificacaoFilter.class.php');

$dao = new TemporadaDao();

if (isset($_GET['id_temporada']) && isset($_GET['id_categoria'])) {

	$filter = new ClassificacaoFilter($_GET['id_temporada'], null, $_GET['id_categoria']);
	if (isset($_GET['id_etapa'])) {
		$filter->idEtapa = $_GET['id_etapa'];
	}
    $result = $dao->listRanking($filter);

} else if (isset($_GET['id_temporada']) && isset($_GET['team'])) {
	$filter = new ClassificacaoFilter($_GET['id_temporada'], null, null);
    $result = $dao->listRankingTeam($filter);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>