<?php

require_once('EquipeDao.class.php');
require_once('EquipeVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $vo = $request;
    $dao = new EquipeDao();
    $result = $dao->saveEquipe($vo);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>  