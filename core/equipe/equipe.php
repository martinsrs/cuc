<?php

require_once('EquipeDao.class.php');
require_once('EquipeVO.class.php');

$dao = new EquipeDao();
$vo = new EquipeVO();

if (isset($_GET['id_equipe'])) {
    $vo->idEquipe = $_GET['id_equipe'];
} else {
    $result = $dao->listAllEquipes();    
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>