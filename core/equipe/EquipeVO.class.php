<?php

class EquipeVO {
    
    public $idEquipe;
    public $nome;
    public $cidade;
    public $estado;    
    
    public function loadFromResultSet($rs) {
        $this->loadFromResultSetSuffix($rs, "");
    }
    
    public function loadFromResultSetSuffix($rs, $suffix) {
        if (isset($rs['id_equipe'.$suffix])) {
            $this->idEquipe = $rs['id_equipe'.$suffix];
        } else if (isset($rs['id_equipe'])) { 
            $this->idEquipe = $rs['id_equipe']; 
        }
        if (isset($rs['nome'.$suffix])) { $this->nome = $rs['nome'.$suffix]; }
        if (isset($rs['cidade'.$suffix])) { $this->cidade = $rs['cidade'.$suffix]; }
        if (isset($rs['estado'.$suffix])) { $this->estado = $rs['estado'.$suffix]; }
    }
    
}


?>