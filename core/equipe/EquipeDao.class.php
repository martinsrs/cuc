<?php
require_once('../db.inc.php');
require_once('EquipeVO.class.php');

class EquipeDao {
    
    private $SELECT_ALL_TEAMS = "select id_equipe, nome, cidade, estado from equipe order by nome";
    private $INSERT_TEAM = "insert into equipe (nome, cidade, estado) values (?, ?, ?)";
    private $UPDATE_TEAM = "update equipe set nome=?, cidade=?, estado=? where id_equipe=?";
    private $DELETE_TEAM = "delete from equipe where id_equipe=?";

    private function getListFromRS($rs) {
        $list = array();        
        foreach ($rs as $val) {
            $equipe = new EquipeVO();
            $equipe->loadFromResultSet($val);
            array_push($list, $equipe);            
        }        
        return $list;
    }
    
    /**
        Lista todas Equipes
    */
    public function listAllEquipes() {
        $result = Db::executeQuery($this->SELECT_ALL_TEAMS, null);
        return $this->getListFromRS($result);
    }
    
    /**
        Insere ou salva uma nova equipe
    */
    public function saveEquipe($vo) {
        
        $query = $this->INSERT_TEAM;

        if (isset($vo)) {

            $params = array($vo->nome, $vo->cidade, $vo->estado);
            
            if (isset($vo->idEquipe) && $vo->idEquipe != null) {
                $query = $this->UPDATE_TEAM;
                array_push($params, $vo->idEquipe);
            }
            
            return Db::executeSave($query,$params);
        }
        
        return false;
    }
    
    /**
        Exclui a equipe
    */
    public function deleteEquipe($vo) {
        $query = $this->DELETE_TEAM;
        
        if (isset($vo)) {
            if (isset($vo->idEquipe) && $vo->idEquipe != null) {
                $params = array($vo->idEquipe);
                $result = Db::executeSave($query,$params);
                                
                return $result;
            }
        }
        
        return false;
    }
    
}


?>