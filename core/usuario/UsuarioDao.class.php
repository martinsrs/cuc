<?php

require_once('../db.inc.php');


class UsuarioDao {

	private $LIST_USUARIO = "select id_usuario, nome, email from usuario order by nome";
	private $GET_USUARIO = "select id_usuario, nome, email from usuario where id_usuario = ?";
	private $INSERT_USUARIO = "insert into usuario (nome, email, senha) values (?, ?, ?)";
	private $UPDATE_USUARIO = "update usuario set nome=?, email=?, senha=? where id_usuario = ?";
	private $LOGIN_USER     = "select id_usuario, nome, email from usuario where email=? and senha=?";

	private function getListFromRS($rs) {
        $list = array();
        foreach ($rs as $val) {
            $vo = new UsuarioVO();
            $vo->loadFromResultSet($val);
            array_push($list, $vo);            
        }
        return $list;
    }
    
	public function getUsuario($vo) {
		$result = Db::executeQuery($this->GET_USUARIO, array($vo->idUsuario));
		return $this->getListFromRS($result);
	}

	public function login($vo) {
		$result = Db::executeQuery($this->LOGIN_USER, array($vo->email, md5($vo->senha)));
		return $this->getListFromRS($result);
	}


	public function saveUser($vo) {
		$params = array();
		array_push($params, $vo->nome);
		array_push($params, $vo->email);
		array_push($params, md5($vo->senha));

		if (isset($vo->idUsuario)) {
			array_push($params, $vo->idUsuario);
			$result = Db::executeSave($this->UPDATE_USUARIO, $params);
		} else {
			$result = Db::executeSave($this->INSERT_USUARIO, $params);
		}

		return $result;
	}
}

?>