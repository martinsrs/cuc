<?php
require_once('UsuarioDao.class.php');
require_once('UsuarioVO.class.php');

$dao = new UsuarioDao();
$vo = new UsuarioVO();


$result = false;
if (isset($_GET['id_usuario'])) {
    $vo->idUsuario = $_GET['id_usuario'];
    $result = $dao->getUsuario($vo);
} 

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>