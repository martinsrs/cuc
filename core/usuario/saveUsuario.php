<?php

require_once('UsuarioDao.class.php');
require_once('UsuarioVO.class.php');

$postdata = file_get_contents("php://input");
$request = json_decode($postdata);

$result = false;

if ($request) {
    $vo = $request;  

    $dao = new UsuarioDao();
    $result = $dao->saveUser($vo);
}

header('Content-Type: application/json');
echo $json_response = json_encode($result);

?>