<?php
require_once('../db.inc.php');

class UsuarioVO {

	public $idUsuario;
	public $nome;
	public $email;
	public $senha;


	public function loadFromResultSet($rs) {
        
        if (isset($rs['id_usuario'])) { $this->idUsuario = $rs['id_usuario']; }
        if (isset($rs['nome'])) { $this->nome = $rs['nome']; }
        if (isset($rs['email'])) { $this->email = $rs['email']; }
        if (isset($rs['senha'])) { $this->senha = $rs['senha']; }
    }

}

?>