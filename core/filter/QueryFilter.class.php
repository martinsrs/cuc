<?php

class QueryFilter {
	
	public $pageSize;
	public $currentPage;


	public function QueryFilter($pageSize, $currentPage) {
		$this->pageSize = $pageSize;
		$this->currentPage = $currentPage;
	}

	public function checkFields() {
		if ($this->pageSize == null) {
			return false;
		}
		if ($this->currentPage == null) {
			return false;
		}
		return true;
	}

	public function getPage() {

		$page = 0;
		for ($i = 0; i < $this->currentPage; $i++) {
			$page = $page + $this->pageSize;
		}

		return $page;
	}

}

?>