<?php

require_once('adodb5/adodb.inc.php');

class Db {
    
    private static $servername = "localhost";
    private static $username = "root";
    private static $password = "";
    private static $database = "cuc";
    
    private static function getConnection() {
        $server = (!getenv("OPENSHIFT_MYSQL_DB_HOST")) ? self::$servername : getenv("OPENSHIFT_MYSQL_DB_HOST");
        $user = (!getenv("OPENSHIFT_MYSQL_DB_USERNAME")) ? self::$username : getenv("OPENSHIFT_MYSQL_DB_USERNAME");
        $pass = (!getenv("OPENSHIFT_MYSQL_DB_PASSWORD")) ? self::$password : getenv("OPENSHIFT_MYSQL_DB_PASSWORD");

        $db = ADONewConnection('mysqli'); # eg. 'mysql' or 'oci8'
        $db->Connect($server, $user, $pass, self::$database);
            
        return $db;
    }
    
    public static function executeQuery($query, $params) {
        
        $db = Db::getConnection();
        
        if ($params) {
            $rs = $db->Execute($query, $params);            
        } else {
            $rs = $db->Execute($query);
        }
        
        if ($db->debug) {
            self::debug($rs->GetRows());
        }
        
        return $rs->GetRows();
    }
    
    public static function executeSave($query, $params) {
        
        $db = Db::getConnection();
        
        if ($db->Execute($query, $params) === false) {
            return false;
        }
        
        return $db->GetOne("SELECT LAST_INSERT_ID()");
    }
    
    public static function executeDelete($query, $params) {
        
        $db = Db::getConnection();
        
        if ($db->Execute($query, $params) === false) {
            return false;
        }
        
        return true;
    }
    
    public static function debug($value) {
        print "<pre>";
        print_r($value);
        print "</pre>"; 
    }
    
}

?>