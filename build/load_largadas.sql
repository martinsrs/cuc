/*
-- Query: SELECT * FROM cuc.etapa_largada
LIMIT 0, 1000

-- Date: 2016-11-18 16:33
*/

INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,2, 1,'11h45m','1h + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,12,1,'09h00m','35min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,14,1,'11h45m','50min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,23,1,'10h45m','45min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,24,1,'09h00m','40min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,25,1,'09h00m','15min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,26,1,'09h00m','20min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,27,1,'09h00m','30min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,28,1,'09h00m','25min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,29,1,'10h30m','500m');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,30,1,'10h30m','1 volta');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,31,1,'10h45m','40min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,32,1,'10h45m','30min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,33,1,'10h45m','35min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,34,1,'10h45m','25min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,36,1,'11h45m','35min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,37,1,'11h45m','40min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,38,1,'11h45m','1h15min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,41,1,'10h45m','20min + 2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,42,1,'10h00m','2 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,43,1,'10h00m','4 voltas');
INSERT INTO `etapa_largada` (`id_etapa`,`id_categoria`,`id_temporada`,`horario_largada`,`tempo_prova`) VALUES (1,44,1,'10h00m','3 voltas');
