#!/bin/bash

echo -en "\n\n\n------------------------------------------------------------\n"
echo " D E P L O Y M E N T"
echo -en "\n------------------------------------------------------------\n\n\n"

echo "Setting env vars..."
echo
UPSTREAM_SSH="ssh://581394fe2d52711be1000152@copauniaodeciclismo-${OPENSHIFT_NAMESPACE}.rhcloud.com/~/git/copauniaodeciclismo.git/"
BITBUCKET_REPO="https://martinsrs@bitbucket.org/martinsrs/cuc.git"
APP_DIST_PATH="/var/lib/openshift/58177f3889f5cf065f00010e/app-root/runtime/app_dist"

echo ""
echo "WORKSPACE: ${WORKSPACE}"
echo "APP_DIST_PATH: ${APP_BUILD_PATH}"
echo "APP_DIST_PATH: ${APP_DIST_PATH}"
echo "UPSTREAM_SSH: ${UPSTREAM_SSH}"
echo "BUILD TAG: ${BUILD_TAG}"

cd "${WORKSPACE}"
echo
echo "Removing old workspace files..."
echo
rm -rf "${APP_DIST_PATH}"

echo -en "\n------------------------------------------------------------\n"
echo
echo "Cloning server files..."
echo

count=1
max=6

while [ $count -lt ${max} ]
do 
    echo "Attempt ${count} of ${max}"

    count=$((count+1))
    rm -rf "${APP_DIST_PATH}"
    git clone "${BITBUCKET_REPO}" "${APP_DIST_PATH}"

    GIT_EXIT_CODE=$?

    cd "${APP_DIST_PATH}"
    ls

    if [ "${GIT_EXIT_CODE}" == "0" ]; then
    	count=${max}
    fi

done

cd "${WORKSPACE}"

if [ "${GIT_EXIT_CODE}" == "0" ]; then
	echo
	echo "Pushing files to server..."
	echo

	git checkout master
	git pull
	
    cp -Rvf "${APP_DIST_PATH}/*" "${WORKSPACE}"

	git add -A
	git commit -m "Adding new version of 'Copa Uniao De Ciclismo' - build tag: ${BUILD_TAG}"
	git push
else 
	echo "Failed while cloning repository!" && exit 1
fi


[ ${GIT_EXIT_CODE} != "0" ] && echo "Failed while cloning repository!" && exit 1

cd "${WORKSPACE}"

cp -Rvf "${WORKSPACE}/*" "${APP_DIST_PATH}"
cd "${APP_DIST_PATH}"

echo
echo "Pushing files to server..."
echo

git add -A
git commit -m "Adding new version of TEIA UI - build tag: ${BUILD_TAG}"
git push 



----


source $OPENSHIFT_CARTRIDGE_SDK_BASH

alias rsync="rsync --delete-after -azS -e '$GIT_SSH'"

upstream_ssh="581394fe2d52711be1000152@copauniaodeciclismo-${OPENSHIFT_NAMESPACE}.rhcloud.com"

# remove previous metadata, if any
rm -f $OPENSHIFT_HOMEDIR/app-deployments/current/metadata.json

if ! marker_present "force_clean_build"; then
  # don't fail if these rsyncs fail
  set +e
  rsync $upstream_ssh:'$OPENSHIFT_BUILD_DEPENDENCIES_DIR' $OPENSHIFT_BUILD_DEPENDENCIES_DIR
  rsync $upstream_ssh:'$OPENSHIFT_DEPENDENCIES_DIR' $OPENSHIFT_DEPENDENCIES_DIR
  set -e
fi

# Build/update libs and run user pre_build and build
gear build

# Run tests here

# Deploy new build

# Stop app
$GIT_SSH $upstream_ssh "gear stop --conditional --exclude-web-proxy --git-ref $GIT_COMMIT"

deployment_dir=`$GIT_SSH $upstream_ssh 'gear create-deployment-dir'`

# Push content back to application
rsync $OPENSHIFT_HOMEDIR/app-deployments/current/metadata.json $upstream_ssh:app-deployments/$deployment_dir/metadata.json
rsync --exclude .git $WORKSPACE/ $upstream_ssh:app-root/runtime/repo/
rsync $OPENSHIFT_BUILD_DEPENDENCIES_DIR $upstream_ssh:app-root/runtime/build-dependencies/
rsync $OPENSHIFT_DEPENDENCIES_DIR $upstream_ssh:app-root/runtime/dependencies/

# Configure / start app
$GIT_SSH $upstream_ssh "gear remotedeploy --deployment-datetime $deployment_dir"

----





echo -en "\n\n\n------------------------------------------------------------\n"
echo " D E P L O Y M E N T"
echo -en "\n------------------------------------------------------------\n\n\n"

echo "Setting env vars..."
echo
BITBUCKET_REPO="https://martinsrs@bitbucket.org/martinsrs/cuc.git"
APP_DIST_PATH="${WORKSPACE}/cuc"
OPENSHIFT_REPO_DIR="${WORKSPACE}/copauniaodeciclismo"
echo ""
echo "WORKSPACE: ${WORKSPACE}"
echo "APP_DIST_PATH: ${APP_DIST_PATH}"
echo "BUILD TAG: ${BUILD_TAG}"
echo "OPENSHIFT_REPO_DIR: ${OPENSHIFT_REPO_DIR}"

cd "${WORKSPACE}"
echo
echo "Removing old workspace files..."
echo
[[ -d ${APP_DIST_PATH} ]] && rm -Rf "${APP_DIST_PATH}"

echo -en "\n------------------------------------------------------------\n"
echo
echo "Cloning server files..."
echo

count=1
max=6
#git config --global push.default matching
while [ $count -lt ${max} ]
do 
    echo "Attempt ${count} of ${max}"

    count=$((count+1))
    git clone "${BITBUCKET_REPO}" "${APP_DIST_PATH}"

    GIT_EXIT_CODE=$?

  if [ "${GIT_EXIT_CODE}" == "0" ]; then
      count=${max}
    fi

done

cd "${APP_DIST_PATH}"

if [ "${GIT_EXIT_CODE}" == "0" ]; then
  echo
  echo "Pushing files to server..."
  echo

  git checkout master
  git pull
   
  cd "${WORKSPACE}"   
  cp -Rf ${APP_DIST_PATH}/* "${OPENSHIFT_REPO_DIR}"
  rm -rf "${APP_DIST_PATH}"
  cd "${OPENSHIFT_REPO_DIR}"  
  
  git add -A
  git commit -m "Adding new version of 'Copa Uniao De Ciclismo' - build tag: ${BUILD_TAG}"
    git push origin master
else 
  echo "Failed while cloning repository!" && exit 1
fi