-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: cuc
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (2,'Master A','30 a 39 anos',12),(12,'Amador','CritÃ©rio TÃ©cnico',7),(14,'Master B','40 a 49 anos',13),(23,'Master C','51 a 59 anos',14),(24,'ForÃ§a Livre','CritÃ©rio TÃ©cnico',9),(25,'Infanto Juvenil','10 a 13 anos',3),(26,'Juvenil','10 a 16 anos',4),(27,'Feminino','CritÃ©rio TÃ©cnico',6),(28,'Veteranos B','70 anos em diante',16),(29,'Fraldinha','4 a 6 anos',1),(30,'Infantil','7 a 9 anos',2),(31,'Estreante','CritÃ©rio TÃ©cnico',8),(32,'Mountain Bike','Pneus mÃ­nimos 1.5 permitidos',17),(33,'Veteranos A','60 a 69 anos',15),(34,'Mountain Bike Veteranos','Pneus mÃ­nimos 1.5 permitidos - 40 anos em diante',18),(35,'JÃºnior','17 e 18 anos',5),(36,'Sub 23','19 a 22 anos',10),(37,'Sub 30','23 a 29 anos',11),(38,'Elite','CritÃ©rio TÃ©cnico',19);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'cuc'
--
/*!50003 DROP PROCEDURE IF EXISTS `ajust_order` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ajust_order`()
BEGIN
	DECLARE v_id_categoria int;
    DECLARE v_ordem int;
	DECLARE done INT DEFAULT 0;
	DECLARE cursor_cat CURSOR FOR select id_categoria from categoria order by ordem asc;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
	
    set v_ordem = 1;
    open cursor_cat;
    read_loop: LOOP
		FETCH cursor_cat into v_id_categoria;
		
        IF done = 1 THEN
			LEAVE read_loop;
		END IF;
        
		UPDATE CATEGORIA SET ORDEM = v_ordem where id_categoria = v_id_categoria;
        
        set v_ordem = v_ordem + 1;
            
    END LOOP read_loop;
	close cursor_cat;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `move_down_category` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `move_down_category`(in p_movedown_id int)
BEGIN
	DECLARE	v_down_ordem int;
    DECLARE v_up_ordem int;
    DECLARE v_up_id int;
    
    select ordem, id_categoria into v_down_ordem, v_up_id
    from categoria
		where id_categoria in (
		select b.id_categoria
		from categoria a
		left outer join categoria b on (b.ordem = a.ordem+1)
		where a.id_categoria = p_movedown_id
	);
    
    IF(v_down_ordem IS NOT NULL) THEN
    
		select ordem into v_up_ordem
		from categoria
		where id_categoria = p_movedown_id;
		
		update categoria set ordem = v_down_ordem where id_categoria = p_movedown_id;
		update categoria set ordem = v_up_ordem where id_categoria = v_up_id;
        
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `move_up_category` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `move_up_category`(in p_moveup_id int)
BEGIN
	DECLARE	v_up_ordem int;
    DECLARE v_down_ordem int;
    DECLARE v_movedown_id int;
    
    select ordem, id_categoria into v_up_ordem, v_movedown_id
    from categoria
		where id_categoria in (
		select b.id_categoria
		from categoria a
		left outer join categoria b on (b.ordem = a.ordem-1)
		where a.id_categoria = p_moveup_id
	);
    
    IF(v_up_ordem IS NOT NULL) THEN
    
		select ordem into v_down_ordem
		from categoria
		where id_categoria = p_moveup_id;
		
		update categoria set ordem = v_up_ordem where id_categoria = p_moveup_id;
		update categoria set ordem = v_down_ordem where id_categoria = v_movedown_id;
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-09  9:22:46
